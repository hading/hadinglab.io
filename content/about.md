---
title: "About"
date: "2019-01-23"
draft: false
---

This site is made using the [hugo](http://gohugo.io) static site
generator. The theme is Jane with a few customizations. The site
started off its life as the dump of a [Zim](http://zim-wiki.org)
wiki, and it is possible that some of the early things may not
have been ported properly.

There is no particularly serious purpose for this site. 
Writing reviews of books gives me a way to remember what I've
read and find it again should I need to. Writing bloggish posts
may do the same, or may just help me to clear my mind.
