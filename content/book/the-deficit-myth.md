---
title: "The Deficit Myth"
subtitle: "Modern Monetary Theory and the Birth of the People's Economy"
authors:
  - "Stephanie Kelton"
read_by: Stephanie Kelton
date: 2022-01-12T12:22:40-05:00
tags:
  - economics
  - modern monetary theory
categories:
  - audiobook
draft: false
---

I suppose that any discussion of Mondern Monetary Theory (MMT) is
likely to be controversial. It definitely is a relatively new approach
to macroeconomics and is not the orthodox view. Kelton is an economist
that works in MMT. I am not an economist, so please take my comments
with a grain of salt and do some reading like this for a better
view. Moreover it has been a bit since I listend to the book, so my
view may be foggy.

MMT is basically relevant to entities that issue their own currency.
That is, it applies to a country like the United States, but it does
not apply to states in the US, nor to countries in the Eurozone that
use the Euro and cannot issue it.

The key observation is that such entities _cannot run out of money_.
They are in control of issuing the money and can issue as much as they
like. Thus, deficits and the idea of not having enough money to do
things are wholly an artificial creation. They simply don't matter.
The fact that so many people buy into the idea (whether by ignorance
or because having that idea be believe is useful to their priorities)
that they _do_ matter prevents a realistic evaluation of what can be
done.

Kelton is quick to point out that this doesn't mean that deficits
_should_ be run or that there are no limitations. The point is more
that there should be discussion about what the society wants to
achieve and what the real limitations are, like worker availability,
natural resources, and inflation, and that fiscal and monetary policy
should be directed to maximizing societal gain while respecting those
limitations rather than the artifical one of having an arbitrary
deficit policy.

I had understood some of these ideas (and will admit I am sympathetic
to them) before reading the book. One of the main questions that it
answered for me is - then why do people value this sort of money? The
answer that I had been familiar with is that it's just kind of a trust
in the system, that there is just kind of general societal agreement
that we're going to use it and have confidence in it. The MMT answer
that Kelton gives is not quite so loose - the currency has value
because the issuer _has the ability to demand payment in the
currency_.  That is, the government has the right to demand that you
pay taxes in its currency and can penalize you if you don't. That
actually gives the currency tangible value.

So what would be the value of running the economy the way MMT
suggests?  An example would be removing involuntary unemployment. The
government could establish a program where anyone who wanted a job
would have one at some set base rate of pay. Money does not need to be
found to do this - we decide the policy goal, establish if the real
resources (in this case people and natural resources to do the jobs
that would be created) exist, and then just do it.

Am I an economist? No, as stated earlier. Do I find this kind of
reasoning compelling? Fairly well so. Are there detractors?
Absolutely, and you can find their stuff and read it as well. Does
Kelton provide evidence that these ideas can work and that they can
avoid problems that other economic theories cause? I think so, but
again, I'm not the best to evaluate here.

I will reemphasize something that I've said elsewhere before. Those
who would have you believe that there is some canonical, natural
economic system are clearly full of it. You just don't get to avoid
responsibility for the economic system that you implement and pains
that it causes to real humans by claiming that there is. That is maybe
a meta-fact that needs to be fought for in the minds of people in
order to avoid laziness in this and to have real discussions about how
different economic systems will work and what they will achieve.  I
don't expect that people will then agree on what to do, but at least
we'll be honest about how the economic policies that we implement will
affect people and that we could have done otherwise, with different
effects. And the discussion can lie where it should - on what we as a
society want to achieve and how to mediate these _goals_ between
different people and groups, and then only once _that_ is achieved
figure out how to organize an economy to get there.

