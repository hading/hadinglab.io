---
title: "The Immoral Majority"
immsubtitle: "Why Evangelicals Chose Political Power Over Christian Values"
authors:
  - "Ben Howe"
read_by: Marc William
date: 2020-01-15T09:09:32-05:00
tags:
  - politics
  - Evangelical Christianity
categories:
  - audiobook
draft: false
---

I got this book from the library based only upon its title, which
easily could be indicative of authorship by a secular source or a
liberal religious view matching my own. However, Ben Howe turns out to
be a right wing voice lamenting his party's embrace of Donald Trump
and more generally the loss of their stated moral stance in the
pursuit of wordly power.

Well, clearly I decided to listen to the book anyway. There are plenty
of interesting bits. Comparison of the stances (and quotes from) the
selfsame religious leaders upon the ascension of Trump vs. the Bill
Clinton era are revealing. We follow Howe's journey from the son of a
pastor (who opined that the reason a lot of people didn't like
Christians were because - and I might use a different word here - many
Christians are jerks) into the conservative movement and into the
eventual realization that maybe what most of the rest of us have seen
for a long time is true - that it is about wordly power.

That said, though he voted for a third party, he remains a
conservative and as such someone like me is going to think that he
still hasn't really seen that the aberration is innate to conservatism
itself. I find it laughable to present moderan American conservatism
as a philosophy that concerns itself with facts in opposition to
liberalism. True, that is where relativists are likely to wind up, but
much more common, I think, is the notion that we only partially
understand the truth and that it may be more subtle than we have
realized to this point, and that we can expose that subtlety. Or, as I
like to call it, science.

I also obviously take issue with the stance that Christianity is
defined by a particular interpretation of the Bible (or indeed, that
it must necessarily be based on the Bible in the first place). Indeed,
this seems to make sense only if there is some such canonical
interpretation in the first place. That seems unlikely to me for a
variety of reasons, but even if we grant that it exists, then it
remains to show that it is actually accessible to us, and that seems
to me a great problem as well.

That's just a taste. If you're curious how a segment of contemporary
Christians may have been led down a path that to many of us seems
decidedly non-Christian (and they might level the same charge at me,
but as I mentioned, I think there are subtleties that can explain
this) then this book might be interesting. If you thought you might
find a greater disavowal of conservatism, this is not that. There is
always hope that the former might lead to the latter, though. At one
point in my life I was a lot more conservative than I am now.

