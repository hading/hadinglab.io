---
title: "Banach Algebra Techniques in Operator Theory"
subtitle: ""
authors:
  - "Ronald G. Douglas"
date: 2023-10-22T10:43:07-04:00
tags:
  - mathematics
  - analysis
  - functional analysis
  - operator theory
  - Hardy spaces
categories:
draft: false
---

I had read part of this book before. The first three, I believe (it might have been four, but I think three), chapters constituted the material for the minor part of my oral exam. The choice of this book was not surprising, given that the minor was in complex functional analysis and my minor advisor was Ron Douglas. At that time I was to read the first three chapters over the course of a couple semesters and submit some selection of the problems to Ron to see that I was getting things. I guess I did, as though I did pass orals I think I did a much better job with this part of it than with my major topic of Teichmueller theory. Maybe that's not surprising - I've probably always been a better analyst than geometer and I probably should have just been willing to admit that to myself beforehand. I did eventually wind up switching back, albeit not directly in this area.

I had always been interested in learning some more about C-* algebras than I ever got to in graduate school, but before attempting to tackle some more specific books in that area I thought it wise to do some lower level review. When I was in grad school Ron had just lent me a copy of this book, but this spring Springer was selling it at a deep discount so I got a hold of it (and some others!) as it seemed like a great way to get going.

There are seven chapters. The first five are a course in functional analysis covering Banach spaces, Banach algebras, Hilbert space, and operators on Hilbert space (which of course relate heavily to Banach algebras). I was pleased that I could still conquer a good number of the exercises, though honestly not as well as I could when I was steeped in this stuff. Good enough for this as a hobby, though. In some ways I'd say these are idiosyncratic, at least a bit, as they are aimed at facilitating one's understanding of the last two chapters, not giving a general picture of introductory functional analysis (like, say, Conway's "A Course in Functional Analysis"). 

The last two chapters are about Hardy spaces and Toeplitz operators from the perspective of the material covered in the first five. I'd always been interested in reading through these, but never gotten to it. As I write this I still haven't gone through that part, but I believe these were some of the considerations that led to the Brown-Douglas-Fillmore theory that was one of Ron's major accomplishments and that was something of a revolution in operator theory and operator algebras. That is not covered in this book, but perhaps I'll push through elsewhere and at least get a taste of it. As I understand it involves using algebraic-topological and homological algebra techniques to study operator algebras. But also Toeplitz operators are of interest independently - they are a more studied compatriot of the compostion operators that I was interested in, and in particular one thing that I tried to produced results on without success (projected composition operators) was a fairly direct analog of the relationship between multiplication operators and Toeplitz operators. 

The book may be fifty years old now, but I think it holds up as a guide to the material that it aims to present. I'm looking forward to finishing the rest of it and seeing how well I can do on exercises that are not quite so familiar to me. We'll see. Then perhaps on to some more specialized material on C-* algebras.