---
title: "The Player of Games"
subtitle: ""
authors:
  - "Iain Banks"
date: 2020-02-03T11:02:33-05:00
tags:
  - science fiction
categories:
draft: false
---

The second Iain Banks Culture novel, about a gamesman in the Culture
(a future post-scarcity society of both biological beings and
machines) who competes in the ritual game of an outside empire that
determines the roles the participants will play in that society. Space
opera-y, but the interest for me is a common theme (I think) in these
novels, namely how people search for meaning in their lives in this
sort of society.
