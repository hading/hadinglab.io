---
title: "The Prodigal Tongue"
subtitle: "The Love-Hate Relationship Between American and British English"
authors:
  - Lynne Murphy
date: 2019-02-15T10:31:26-06:00
tags:
  - english
  - language
  - linguistics
draft: false
---

This is a delightful little book whose focus is pretty well conveyed
by its title. One might consider the central organizing principle to
be the British Verbal Superiority Complex versus the American Verbal
Inferiority Complex, but it is much more than just that.

The book, written by an American linguistics professor who is a long
time expatriate in England, goes well beyond the shallow differences
that we often think of when comparing these two "nationlects" as she
likes to call them, such as the extra 'u's that seem to pop up in
British words (from my American perspective) or talking of lifts
instead of elevators. It's a fun romp through linguistic differences
and exploration of how the two nationlects influence each other. It's
even a fun exploration of when they haven't influenced each other,
even though one side or the other thinks they have. (For example, the
English are likely to blame anything they don't like about their
language on America, even if it came from say Australia, New Zealand,
or even England. Or, Americans are likely to think that anything
sufficiently funny sounding came from England, like the word
'bumbershoot'. Murphy reveals that Brits have no idea what that is -
in fact it came from an American film about English people,
Chitty-Chitty Bang-Bang.)

The particulars of the examples are interesting, as are the historical
stories. It is fun to read about Americans, as their country emerged,
attempting to stake out their own form of the language, or of the
British trying to prevent the (perceived) corruption of their language
by American influence. An interesting idea for me was how words do
tend to cross over when there is a useful niche of meaning that they
can fill, even though sometimes filling that niche means that they
have to narrow, expand, or alter their meaning a bit - or a lot.

If you like language and find it interesting to think about then it's
easy to recommend this book. I think that many of us in particular
revel in English's ability to adapt and borrow from so many places and
be such a Wild West of language. 
