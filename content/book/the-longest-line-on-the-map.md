---
title: "The Longest Line on the Map"
subtitle: "The United State, the Pan-American Highway, and the Quest to Link the Americas"
authors:
  - "Eric Rutkow"
date: 2019-03-27T09:47:47-05:00
tags:
  - history
  - railroads
  - roads
categories: 
draft: false
---

This is a history the the Pan-American highway. It starts out by telling
a related tale, that of the attempts to make a Pan-American railway. This
occupies roughly the first third of the book. Then there is a brief 
interlude that addresses the birth of modern road building and some
of the technical aspects thereof. Then the story of the actual highway
begins.

The author brings out the various factors going into these stories. There
is political intrigue, both domestic (to the US and to the other American
countries) and international, for example the constant tension for the
Central American countries between obtaining American aid for modernization
and economic progress and fear of American imperialism, either economic or
politcal. On the American side we see the same sorts of factors playing out
in reverse - business opportunities, the vision of a hemisphere led by America,
and reducing the influence of Europe in South America for example on one side,
with factors such as the advisability of spending American money on this
kind of project on the other. In short, perhaps, the same kinds of 
debates that we see today.

Along the way we meet a variety of characters - politicians, businessmen, 
scientists, and engineers who play into this story. Some of them were 
acting strictly on their own interests, some with a view towards
accomplishing more noble goals, and points between.

I did find the fact that the author tends to tell one story to completion
and then move to the next sometimes led me to be confused about the 
overall chronology of events. I would also have liked to have seen maps
more directly connected to the surrounding text. These are small issues, 
though, for a book that tells an interesting if somewhat specialized 
story.

The Pan-American Highway has not been finished. There is still a gap
of about 50 miles or so between Panama and Columbia, and it may 
remain that way, as environmental concerns and the rights of indigenous
peoples have come to the fore, as well as continued concerns about the
other possible negative aspects of completing the road.


