---
title: "How to Argue With a Racist"
subtitle: "What Our Genes Do (and Don't) Say About Human Difference"
authors:
  - "Adam Rutherford"
read_by: Adam Rutherford
date: 2022-01-12T12:23:49-05:00
tags:
  - racism
  - genetics
  - biology
categories:
  - audiobook
draft: false
---

As sometimes happens I got a bit of a surprise with this book. Having
selected it from Libby with only a glance at the title, not even the
subtitle, I expected something perhaps more rhetorical in nature. That
is, perhaps strategies for actually arguing with a racist in a
rhetorical sense, strategies for argumentation without provoking
digging in, etc.

That's not what I got. The book is really about why race as a
biological construct is nonsense. As you might expect, this is also of
interest to me. Rutherford is an researcher into genetics from England
and of Indian descent. The book is primarily about laying out the
factual information from genetics that makes the idea that race is
genetic in nature absurd.

Given that we do have race as a concept in society, it seems to follow
then that it is a socially constructed thing and that when someone
ascribes certain properties to certain races it must therefore come
from a cultural context.

An example, which I hope I'm remembering well enough given that I read
(or listened) to this some time ago is in the area of
sports. Rutherford naturally accepts that _individual_ genetic
variation has a lot to do with success in sports. There is no question
that I was never destinted to be a great basketball player, for
example. However, this simply does not transfer to race, and
differences seen here come about in his view, a quite convincing one,
for cultural reasons.

For example the reason that a lot of world class long distance runners
come from certain parts of Africa is that those regions have cultural
factors that cause that. Their past and current success inspire people
in those areas to take up and value that sport. The training expertise
and facilities are good there and geared to that sport. And so on.

I found the book interesting. I don't know, for the purposes of
American racism, how _useful_ I think it is. That is, I'm not sure
that knowing this stuff is going to enable you to change minds. I'm
not sure that majority of racists are going to accept the facts or
even if they do follow them to the logical conclusions. Still, maybe
some would, and the genetics is interesting enough on its own merits.
