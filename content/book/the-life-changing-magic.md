---
title: "The Life Changing Magic of Not Giving a F*ck"
subtitle: "How to Stop Spending Time You Don't Have with People You Don't Like Doing Things You Don't Want to Do"
authors:
  - "Sarah Knight"
read_by: Sarah Knight
date: 2020-02-14T09:23:00-05:00
tags:
  - life advice
categories:
  - audiobook
draft: false
---

If you look carefully you might be able to discern a certain
similarity in the title of this book to another that is popular around
this time. I wouldn't necessarily say this is a parody, but I bet it
does take a certain inspiration (I haven't read the other one) and
indeed is about tidying up your life in a different way. It's almost
surely more irreverent and moreover f-bombs are dropped more than in
an episode of _Trailer Park Boys_.

Specifically, it is about reclaiming time, energy, and money from
things that you don't care about by stopping worring about what other
people think or you and paring back things that you don't really want
to do. One goal is making this seem a plausible thing to do, to dispel
the myth that you really are obligated to do all of those things or
that there is some terrible consequence awaiting if you give them
up. And of course to guide you in not feeling bad about doing it
(Knight's "Not Sorry Method").

A second goal is to help achieve this without being an ass. It's all
pretty easy to achieve if you're willing to be one, but Knight aims to
show that you can equally well do it without going down that road. You
can be honest and polite about it for the most part and still achieve
the goal of "More joy, less annoy" in your life.

I don't know if the book was super helpful for me, because I already
practice what it preaches in large part. I can imagine that it would
be helpful for others whom I know.

