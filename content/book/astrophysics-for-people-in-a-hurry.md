---
title: "Astrophysics for People in a Hurry"
authors:
  - "Neil DeGrasse Tyson"
read_by: Neil DeGrasse Tyson
date: 2019-10-12T11:43:48-04:00
tags:
  - popular science
  - physics
  - cosmology
categories:
  - audiobook
draft: false
---

I doubt that Tyson needs any defense of his status as a populizer of
science, particularly in his chosen field of astrophysics. This was
one of my first interests in science; in fact stellar evolution (such
as it was understood in the mid-80s, and from a popular standpoint,
not very quantitative) was the topic of my first little ten page or so
term paper in eight grade.

That said, though I was involved in physics for a while I never
studied astrophysics in a serious way, my knowledge here has always
been of a popular nature. Much of the material of this book was at
least familiar to me, though obviously much more is known now than was
to me long ago. When I was young there seemed little hope of actually
finding exoplanets, whereas they turn up all the time with the
techniques and technology now available.

The coverage of dark matter and dark energy was perhaps the most
interesing personally. These are things that thirty five years or so
ago there was little or no inkling of. Yet now they play an imporant
role in cosmology. I have to admit to a prior prejudice against them -
in some sense they seem like an accounting game to make the currently
known laws of physics work out correctly. However, I can then reflect
that you might have said the same of the neutrino, and eventually ways
were found to make _it_ show itself in a convincing manner. You might
have said the same of other elementary particles, atoms, molecules,
etc.

So listening to Tyson's arguments here moved me a bit more in the
direction that this might be going on here, though I also want to
reserve the option that there really is some basic physical notion
that isn't understood. It seems safest to me to say that we just 
don't understand here. 

Anyway, this gives a breezy introduction to a lot of things going on
in astrophysics these days and the attendent enthusiam for science,
observation, evidence, and imagination that go along with it.
