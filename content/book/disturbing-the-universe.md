---
title: "Disturbing the Universe"
authors:
  - Freeman Dyson
date: 2019-02-15T13:18:42-06:00
tags:
  - physics
  - biography
  - Freeman Dyson
draft: false
---

I picked this up at a yard sale. Freeman Dyson is a physicist of the
generation of Richard Feynman, and is perhaps best known for the
concept of a Dyson Sphere (mentioned in this book), the idea that at a
certain time in the development of a civilization they might attempt
to enclose their star entirely so as to use as much of its energy as
possible. In this book he writes of various aspect of his life and
work, and some of his speculations for the future. Given that the book
was published in 1979, it is of some interest to compare what he
expected with the events of the past forty years, though I don't know
how deeply I'll get into that here. Dyson is also clearly quite
interested in poetry, which he seems to think gets to the heart of
many matters, and it is incorporated liberally into the book.

Perhaps the thing that stuck me most was early in the book. Dyson,
born and raised in England, though he would later become an American
citizen, was part of the British bombing efforts during World War II,
working to improve it operationally. For him even to reach this point
was something of an evolution that he details, starting from rather a
pacifistic world view and examining how it changed to allow him to
reach the point where he was doing that job.

Interestingly, his conclusion is that the bombing campaign was pretty
much a complete failure, that the results obtained were nowhere near
justifying the costs incurred. The thing that interested me is perhaps
illustrated in two different ways. First are examples he gives of
improvements that he or his colleagues thought of and the reaction of
the military staff and others to them.

One example had to do with the esacpe hatches in bombers. One of
Dyson's fellow workers did an analysis of these and their usage and
concluded that they were simply too small to be used effectively in
the kinds of situations where they would be needed. To get any action
on this finding, however, required a protracted fight to get the
commanders to recognize or admit that there was a problem, and then to
get changes on new planes being manufactured. Not until near the end
of the war was the change implemented, and many were lost who might
otherwise have been spared.

An analysis by Dyson himself suggested that the gunners inside the
bomber, put there to exchange fire with enemy fighters, actually had
little effect on survival rates. Therefore, it would make sense to
simply remove the gun turrets and gunners, reduce the crew, and spare
the extra men when a plane was shot down. Naturally this challenged
the conventional wisdom and was not received well. Dyson seems to have
had less appetite for advocating for it than his friend, so nothing
every happened. Nevertheless, it illustrates the difficulties with
countering the conventional wisdom of institutions. One aspect of this
difficulty is that the interests of individuals making up institutions
is rarely completely aligned with the goals (even if not corrupted) of
the institution as a whole, making it potentially risky for them
personally to try changes that might improve the institution, even
with the kind of substantiation that Dyson provided with his
study. (And it should be noted here that while I do favor the idea of
challenging conventional wisdom, it needs to be a challenge with
evidence, not merely based on the fact that something is not as one
wishes it to be.)

I think those ideas are pretty well known. What was interesting to me
is it seems related to Dyson's observation that the Bomber Command
exemplified a method of waging war where _no one actually felt
responsible for killing anyone_. In his words (p. 31):

> Technology has made evil anonymous. Through science and technology,
> evil is organized bureaucratically so that no individual is
> responsible for what happens. Neither the boy in the Lancaster aiming
> his bombs at an ill-defined splodge on his radar screen, nor the
> operations officer shuffling papers at squadron headquarters, nor I
> sitting in my little office in the Operational Research Section and
> calulating probabilities, had any feeling of personal
> responsibility. None of us ever saw the people we killed. None of us
> particularly cared.

Dyson seems to find that troubling, and I certainly do. We can
certainly say it is as bad or worse today in the military. How does a
drone pilot feel, not even as physically present as the crew in a
bomber? But even more disturbing is that this pervades our
society. Among the other things that they are, corporations are
vehicles to do exactly what Dyson describes - allow people to do
objectionable things in such a way as they can feel (and in many legal
senses, be) removed from the consequences that these actions
create. This is not a problem that seems to admit an easy solution
unless you are willing to either live with it or define the evil away
(to me an entirely wrong-headed approach which, as anyone who reads my
stuff for a while, will see that I think is commonly used by believers
in a certain stream of economics). I spend a lot of time pondering it,
though I have no expectation that I'll be the one to crack it.

There are a lot of other interesting things in the book. Nuclear
power, quite relevant at that time, is discussed quite a bit. Space
exploration, space travel and speculation about other civilizations
are prominent. There is some biology. The focus is rarely on the
science or technology itself, but rather how it fits into society,
with some element of both practical and philosophical viewpoint. Since
this is an older book it is, as mentioned, interesting to see if
society has been impacted as Dyson thought it would be, or if science
has gone in the direction he believed it would. This is not primarily
to see if he was right or not, but much more so to remind us to be
humble about our own predictions of the future. Dyson is in some ways
a contrarian, and I feel akin to him in that way. It's always crucial
to remember though, I think, that evidence is the root of everything -
there is no value in going contrary to the evidence, but when the
evidence is scant, then we should encourage the development of many
viewpoints while we attempt to collect the evidence.

