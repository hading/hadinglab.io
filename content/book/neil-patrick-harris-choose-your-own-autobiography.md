---
title: "Neil Patrick Harris"
subtitle: "Choose Your Own Autobiography"
authors:
  - "Neil Patrick Harris"
read_by: Neil Patrick Harris
date: 2019-11-06T10:39:04-05:00
tags:
  - memoir
categories:
  - audiobook
draft: false
---

The conceit of this book in print form is that it is written along the
lines of a choose your own adventure book. That is, it is written in
second person, so each section tells what happens to "you". Then, at
the end of each section, you are given choices about what action you
will take next. You are then directed to the next place to continue
reading based on that choice.

That doesn't work so well for an audiobook, so some adaptation is made
for the format and your path is chosen for you, and I will assume it
hits everything in the book. Honestly if I read the book in print I
probably would have just read is straight through anyway to make sure
I didn't miss anything.

I'm not going to pretend that this is an important or deep book. It's
not. It is a breezy celebrity memoir of a likeable celebrity talking
about his lifelong love of show business in many forms, his love of
family, and his long journey coming to terms with his sexuality. In
short, maybe it's a good thing to listen to after something like Al
Franken's treatise on the minutia of everyday political life getting
to and then in the Senate.
