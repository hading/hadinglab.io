---
title: "The Gift of Failure"
subtitle: "How the Best Parents Learn To Let Go So Their Children Can Succeed"
authors:
  - "Jessica Lahey"
read_by: ~
date: 2020-12-05T09:29:22-05:00
tags:
  - parenting
categories:
draft: false
---

It's been a bit since I read this, so I won't attempt to be very
detailed here.  The main point is that by trying to shelter children
from failure we deprive them from having the kinds of experiences and
developing the kinds of skills that they need to truly thrive in life.
Parents who are guility of doing this may also require the kind of
encouragement found here to alter their behaviors and let go a bit.

I take a broader view that in addition we might need to remake society
in such a way that we don't penalize children, or people in general,
from exploring, going down paths that might lead to "failure", and so
forth. Imagine how much richer things could be if we decided how to do
enough to meet people's need and then returned the rest of time and
resources to people so that they could truly explore in ways that they
found compelling, interesting, and fulfilling. Think not only of how
that would impact their direct happiness and well-being, but also what
wonderful things might be discovered in that way rather than in an
attempt to fulfill some economic or political fundamentalism.
