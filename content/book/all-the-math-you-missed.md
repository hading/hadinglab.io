---
title: "All the Math You Missed"
subtitle: "(But Need to Know for Graduate School)"
authors:
  - "Thomas A. Garrity"
date: 2023-10-22T10:42:17-04:00
tags:
 - mathematics
categories:
draft: false
---

The basic idea of this book is to survey the vast array of mathematics that one might have been expected to encounter during an undergraduate career in order to help fill in the inevitable holes in preparation for graduate school. As you might expect, this makes it suitable as well for someone who, say, went to graduate school 25-30 years ago and has some desire to once again study some graduate level mathematics but wants to review things a little bit before jumping right into _that_. 

The second edition, which I was reading, has 20 chapters starting with the like of linear algebra and simple real analysis and venturing through various geometry, topology, analysis, algebra, set theory, etc. Naturally it's impossible to go into much depth in fewer than 400 pages, but many highlights are hit and each chapter contains approachable exercises of which, I'm happy to say, I was still able to do many. There *are* some exercises with incorrect statements, which is perhaps not good for undergraduates, but perhaps good for someone preparing for graduate school. 

You won't know everything you might want to from the undergraduate level just by reading this, but at least you'll have surveyed the landscape and thus have an idea where you might have holes that you want to fill in. Or in my case, a sufficiently low level and broad read in order to start getting my legs under me again.