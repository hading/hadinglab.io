---
title: "How to Think Like an Anthropologist"
subtitle: ""
authors:
  - "Matthew Engelke"
read_by: ~
date: 2021-03-27T07:31:34-04:00
tags:
  - anthropology
categories:
draft: false
---

I know little about anthropology, but feel like I know a bit more
after reading this book. Herein we find examples of the kinds of
things that anthropologists think about and the way that they think
about them. We see a bit about how they do their work and read about
some of the important people in the field and the work that they
did. We get a flavor for the kind of approaches that they take.
