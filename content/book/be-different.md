---
title: "Be Different"
subtitle: "Adventures of a Free-Range Aspergian"
authors:
  - "John Elder Robison"
read_by: ~
date: 2020-12-05T09:29:48-05:00
tags:
  - autism
  - Asperger's Syndrome
  - memoir
categories:
draft: false
---

This is a memoir of a man with Asperger's syndrome who learned to
embrace his differences and thereby make his life. For obvious reasons
it reasonated with me and though the particulars were in many ways
different than for me the overall shape of things was often the same,
resulting in my mentally nodding along as I read.

If you don't have Asperger's but want to add a bit of insight then
this is probably a good way to do that in an entertaining read.
