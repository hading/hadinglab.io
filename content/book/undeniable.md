---
title: "Undeniable"
subtitle: "Evolution and the Science of Creation"
authors:
  - "Bill Nye"
read_by: Bill Nye
date: 2019-10-12T11:44:09-04:00
tags:
  - popular science
  - evolution
  - biology
categories:
  - audiobook
draft: false
---

This book came about as a result of Nye's debate with Ken Ham about
evolution vs. creation. As Nye points out early on, many or even most
theists, unlike Ham, have no problem with evolution, and I suppose I
should be counted as one that thinks that scientific knowledge must
necessarily inform any sort of theism that one holds (and thus that
that must be sufficiently mutable to handle scientific advances).
Thus in any such debate I am firmly on Nye's side. 

But the creationism debate, while running through the book, doesn't
take it over. If you just want a nice popular rundown of the evidence
around evolution as known today, this is a fine place to go. While I
knew who Nye was before listening to the book I don't think I had read
any of his material or really even watched him at any length. He's a
fine populizer and also has a background in comedy that serves well in
making this sort of book. 

I don't feel compelled to say too much about the content. It's what
you would expect for a book that is trying to do the things this one
is. 

