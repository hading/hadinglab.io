---
title: "Modern Php"
subtitle: "New Features And Good Practices"
authors:
  - "Josh Lockhart"
date: 2019-09-02T11:36:56-05:00
tags:
  - programming
  - php
categories:
draft: false
---

This was more of a skim-over than a full read. Often it is hard to
find programming books that hit the level that I want. That is to say,
I've been programming for a long time, and they often contain, for me,
too much information that I already know.

I'll be needed some PHP soon, and I've been casting around for some
references that will be useful. Many of the books I find contain too
much basic information about general programming, databases, HTML,
etc.[^1] Really what I want is something that assumes that I know how to
program and tells me the unique things about PHP.

This book is not exactly that, but it does really do a good job of
hitting things that I wanted to know without explaining to me what a
loop is. That is, I feel that it will help me to use what I know about
everything else in a PHP context. Which is more or less what I'm
looking for. 

Also I'm looking for something that will help me to write both
idiomatically and well. My reaction to another popular book that I
read (now in its fifth edition) was that people couldn't _possibly_
write PHP in such a horrible way. The book at hand has much better
looking code.

I don't think this will be the only book that I'll want to use, but it
will be among them.

[^1]: As another example, when I was learning Clojure I found that
    most of the resources out there were for Java programmers and
    trying to explain the benefits of Lisps and functional
    programming. I was coming from the opposite perspective. I wanted
    to know what was different about Clojure from things like Common
    Lisp, ML and so on.
