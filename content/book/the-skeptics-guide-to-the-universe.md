---
title: "The Skeptics Guide to the Universe"
subtitle: How to Know What's Really Real in a World Increasingly Full of Fake
authors:
  - Steven Novella
  - Bob Novella
  - Cara Santa Maria
  - Jay Novella
  - Evan Bernstein
date: 2019-02-15T13:18:25-06:00
tags:
  - science
  - skepticism
  - reasoning
draft: false
---

This is a breezy read, apparently based on a podcast of the same
name. It was in the new books section at the library and looked
interesting, so I thought I'd pick it up. Skepticism is the main them
of the book. Steven Novella is a medical doctor, so it should be no
surprise that neurological factors are well covered and that examples
related to medicine are prominent.

To me the most valuable part was the first half, dealing largely with
things like proper and improper argument, elements of human psychology
relevant to how we think about things, etc., entitled "Core Concepts
Every Skeptic Should Know". I don't want to go into a lot of details,
as it would be hard to cover everything, but some examples should help
give the flavor of what is going on.

They start by going over the many ways in which things like memory and
perception are, biologically, suspect. Our memories change as time
passes, and it is surprisingly easy for them to alter to accord with
others, or even for false memories to be produced. Our perceptions at
the present are to a large extent infuenced by our brain, which fills
in things that our physical body is unable to sense using its own
model of the world. We tend to miss things. (E.g. the famous 'gorilla
experiment' where subjects watch a video with the mission of counting
something like the number of passes of a ball people make. During the
course of the video, someone in a gorilla costume comes into the
picture and walks around. Something like half the test subjects do not
notice it.)

This then bleeds over into more psychological effects. Dunning-Kruger
is a ready example here, that is the tendency of people who know
little about a subject to vastly overestimate how much they know about
it - although Novella points out that event experts tend to
overestimate their knowledge, though the amount is smaller. Various
types of psychological biases are covered.

Logical fallacies are also covered. Unlike the preceding, which are
endemic to us as humans, though we can recognize and try to limit
their effects, logical fallacies can be carefully reasoned about - and
they can also be used intentionally to deceive (though it is not
always the case that someone employing them is doing so).  The usual
suspects are found here, such as:

* Argument by Ignorance - we can't prove something isn't true, so it is true
* Appeal to Authority - someone is a position of authority says
  something, so it is true, whether or not there is evidence for it
* False Dichotomy - you don't know the answer, hence my answer must be
  right (even though there is no evidence for it either, and excluding
  the very real possibility of other answers)
* Slippery Slope - a slippery slope will be slipped, even though it is
  entirely possible that, well, it won't be
* Strawman - misrepresenting one's opponent's arguments and evidence
  in order to have an easier target to argue against

I can't go through them all, but merely reading and understanding
chapter 10 would be a tremendous improvement to the reasoning ability
of most people. Novella is careul not to remind us to not let
ourselves off the hook - everything he talks about applies equally
well to ourselves, and if we are truly in pursuit of truth rather than
some other end, we need to consciously bring all of this type of
knowledge to bear when we think and constantly check that we ourselves
are not falling into traps in our own thinking. We should also be
generous when considering how others think and know that much here
happens automatically and biologically. We ought to help others
understand these things, but be gentle about it.

If the above sounds interesting, then the other parts of the first
section are likely to be interesting as well, though I'll not go into
them in detail. The last that I'd like to mention is discussion of
chance and coincidence. Given my background in mathematics it is not
surprising that this connects with me. I like to state the main thrust
of it something like this: any given coincidence is unlikely, however,
there are so many possible coincidences, and our brains are such good
machines at looking for them, that not seeing any, or indeed a lot of
them, is itself extremely unlikely.

The relevance here is that we tend to see coincidences and other
unlikely occurences and ascribe (possibly causal) meaning to them that
they do not deserve. The simple fact of the matter is that equally
well something other rare event could have happened and we would then
have tried to ascribe meaning to it. Done properly, one thing that
science is good at is figuring out when there is a real effect and
when we just happen to be seeing a meaningless coincidence. And
indeed, seeing a rare event can trigger a search for a real effect -
but the observation and initial interpretation of the event is not the
place to stop, but the place to start. Most of the time, it will
actually mean nothing.

Once we have some of the basic biological, psychological, and logical
ideas in mind we can then proceed to hit the real world with them. The
rest of the book is devoted to that. Some highlights include
treatments of cold reading, intelligent design, paranormal phenomena,
good and bad general and science reporting, and
pseudoscience/pseudomedicine. One could fairly easily dip into any of
these at will and have an interesting little read.

Whether or not they use this book, I can't recommend highly enough
that people learn the kinds of things it covers about how humans
actually think and about logical fallacies that interfere with good
thinking and apply them to the world. I can only imagine that that
would make things better.

On a personal note it is worth briefly addressing how I advocate this
sort of thing and remain a theist. I don't go looking for supernatural
explanations for things that admit perfectly good natural ones. I
think that the plain evidence of the universe as it exists is not only
true, but is in fact necessary for us to have in order to apply
theistic principles. To wit, the better you understand the universe
the better you are able to apply that knowledge to interact morally
with its other inhabitants. Conversely, if you choose to ignore what
is known and knowable about the universe, willfully or not, then you
diminish your ability to live morally.

As a consequence, I am probably removing any ability to apply
evidenciary procedure to matters of faith, and I'm okay with that. One
might object then that I am falling into a god of the gaps position,
and such a criticism is not entirely unfair, although I do think it is
possible to maintain that there is a real division that is fundamental
and not driven merely by our state of knowledge without being able to
say exactly where it is. (As a mathematical analogy, it may be
possible to prove that a set of numbers has an upper bound without
having any idea what that upper bound may be.) As always, it is
possible that I'm wrong, but here I am.

