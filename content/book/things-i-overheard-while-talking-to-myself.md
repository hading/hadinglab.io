---
title: "Things I Overheard While Talking to Myself"
authors:
  - "Alan Alda"
read_by: Alan Alda
date: 2019-10-12T11:50:40-04:00
tags:
  - memoir
categories:
  - audiobook
draft: false
---

This is largely based on speeches or talks that Alan Alda gave to
various groups - graduating classes at schools, industry groups,
academic groups, and so on. Alda sets up each with some background and
then goes on to the substance of the talk. It wasn't clear to me that
he was recreating the talk as given (I rather had the impression that
this was not the case), but nevertheless the sections are inspired by
specific presentations that he did.

The subjects are vast, from his experiences in theater, television,
and movies to his lifelong interest in science. They include personal
triumphs and failings. They cover many times when he was terrified to
give a talk but agreed anyway, such as when he agreed to say something
about Thomas Jefferson to a group of historians (reading up in
preparation for this he realized - wait a minute, these guys probably
_wrote_ the books that I'm reading). Or, speaking to a graduating
class of medical students, doubtless prompted by his most famous role,
but leaving him wondering what he as an actor who played a doctor
would have to say to a bunch of real ones.

Still in those instances you get the message that being willing to
accept those frightening jobs was also a source of exhiliration and
growth, and that his search for what someone with his particular
experience would even have to say to those audiences conveys the
valuable message that he (and by extension, we) do have something to
contribute to that sort of conversation with the experts. For example,
being Hawkeye Pierce gave him no particular insight in how to be a
doctor, but being Alan Alda gave him insight on being a patient and 
how a patient might want to be treated.

There is also a lot a purely personal advice - for example in college
graduation speeches given at his children's graduations he dispenses
the kind of life advice you might expect a parent to deliver to his
children.

Along the way we learn plenty about his time in show business, his
tremendout pride in promoting science, especially through hosting the
Scientific American Frontiers show, and advocacy for women's rights,
including the hurt he clearly feels over not successfully getting the
ERA passed so many years ago.

Overall an enjoyable listen. As an aside I was unable to resist the
temptation to go back and watch a little bit of M\*A\*S\*H after
listening.  Given the timeframe of my life it's hard for me to
envision a world where M\*A\*S\*H isn't one of my favorite shows of
all time, but some aspects of it, particularly in the early seasons,
just don't play that well in today's world.


