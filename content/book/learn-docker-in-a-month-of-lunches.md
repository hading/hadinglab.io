---
title: "Learn Docker in a Month of Lunches"
subtitle: ""
authors:
  - "Elton Stoneman"
date: 2023-10-22T10:43:33-04:00
tags:
  - computing
categories:
draft: false
---

I don't have a lot to say about this one. It was useful for getting my head around enough of Docker and related technologies to enable me to use it professionally. 

As with all computing books, it is probably a bit out of date by now. But that's okay, as for me it was more of a primer than a reference.