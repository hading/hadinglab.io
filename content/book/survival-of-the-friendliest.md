---
title: "Survival of the Friendliest"
subtitle: "Understanding Our Origins and Rediscovering Our Common Humanity"
authors:
  - "Brian Hare"
  - "Vanessa Woods"
read_by: ~
date: 2021-03-27T07:31:20-04:00
tags: 
  - psychology
  - evolution
  - social cooperation
  - friendliness
categories:
draft: false
---

I went into this expecting it to be primarily about human social issues,
but much of the book is about social cooperation in the animal realm,
though it does get around to humanity as well. It's been a while since
I read the book, so please excuse any fuzziness or inaccuracy.

The authors are experts in dogs and primates. We see a lot about the
domestication of dogs and related animals in particular. One example
of particular interest was the domestication of foxes in an experiment
in Siberia over many years. The researchers selectively bred foxes
based solely on their friendliness to humans and very quickly
selection on this one trait evinced a myriad of differences in the
two populations.

This leads to the idea that certain animals may have in fact
domesticated themselves and even that in a sense we may think of
_humans_ as having domesticated themselves. This can result in the
loss of some traits that we may think of as advantageous for survival,
but the gains in having a cooperative and mutually advantageous
society far outweigh those, a fact often overlooked by those touting
the rugged individualist.



