---
title: "The Gentle Art of Swedish Death Cleaning"
subtitle: "How to Free Yourself and Your Family from a Lifetime of Clutter"
authors:
  - "Margareta Magnusson"
read_by: Juliet Stevenson
date: 2019-10-12T11:45:07-04:00
tags:
  - cleaning
  - decluttering
categories:
  - audiobook
draft: false
---

From the title, one might assume that the advice in this book applies
only to those who are winding down their lives and seeking to do
something with the lifetime of stuff that has accumulated around them.
Indeed, this is the _main_ audience, and as Magnusson says, when you
reach that age you really owe it to those around you to start doing
this sort of thing to prevent those you leave behind from having to
spend a lot of time doing it instead.

However, she is quick to emphasize that it shouldn't be just for those
people - the same basic principles can be used by the young to remove
clutter in _their_ lives as well. This helps prepare for later,
surely, but also there is something to be said for less stuff applying
less pressure to one's present life as well, regardless of age. As
someone who has moved from a fairly large house that managed to take
in a lot of possessions and will hopefully soon be in a somewhat
smaller house, this strikes a chord of something that will have to
happen as we remove things from storage where they now reside (indeed,
we tried to purge much before it went to storage, and yet there is
still much more to do).

I wouldn't say that the book at hand is primarily trying to get at the
nuts and bolts of this kind of operation, though there are plenty of
tips about that as well. I think it is more aimed at getting one into
the state of mind that this is a reasonable and important thing to do,
and let's get on with it. How do we not be oversentimental about our
stuff, how do we decide between keeping, giving away, donating, or
discarding?  What other sorts of riches do we expect to take the place
of material stuff if we free ourselves from it? Those and more are
things to ponder.

Though I've not done it, I've often pondered selling or giving away my
first guitar - not that I don't like it or that it has been superseded
by other instruments, though I do have ones that would be fine to take
its place, but because I feel like I may be too sentimentally attached
to it as my first guitar. I wonder what situation might come along
where I would do so. I think that there are certain people that, if
one asked for it, I would just give it. I wonder what else there might
be.

That is the kind of thing this book might get you started thinking
about.  I hope enough of the lessons persist when the new house is at
hand to make me effective at clearing out more things than I was able
when I packed up the old.
