---
title: "Confucianism and Taoism"
authors:
  - "Julia Ching"
read_by: Ben Kingsley
date: 2019-10-12T11:49:01-04:00
tags:
  - religion
  - philosophy
  - Taoism
  - Confucianism
categories:
  - audiobook
draft: false
---

From the time I took a college course in (translated) Chinese
literature that included some selections from the Chuang-Tsu I've had
some interest in Taoism. Actually before then, even, I had read some
books by Raymond Smullyan, a logician and Taoist. I was more
interested in the logic and mathematics, but I know that some that I
had read then had really leaned into the Taoist philosophy, though I
gave it no thought when I was reading it.

This book then, is an overview of Confucianism and Taoism in both
their philosophical and religious facets. I had kind of hoped that it
would more address the _relationships_ between the two than it does
(the Taoist writings do make fun of Confucians quite a bit), but it is
more of a straight take on the two independently and doesn't go down
that road too much.

Since I've read at least the two big Taoist writings, I was more
interested in Confucianism, about which I knew little. So I think that
this did get me that. Also I'm primarily interested in Taoism as a
philosophical system, so learning a bit about the religious side of it
was informative as well. Along the way we get a little something about
some of the other schools of thought like Moism and so on, but the
focus is primarily on the titular two.

Perhaps sometime I'll write a non-review post about what I think about
Taoism and why I think it is interesting and so forth, and maybe in
general what I think Eastern philosophy has to do with Western
religion.

