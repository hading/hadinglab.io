---
title: "Moral Tribes"
subtitle: "Emotion, Reason, and the Gap Between Us And Them"
authors:
  - "Joshua Green"
date: 2019-03-23T07:23:53-05:00
tags: 
  - moral philosophy
  - psychology
  - neuroscience
categories:
  - top pick
draft: false
---

This is a fascinating book that I had anticipated reading for some time.
It begins by considering what the author calls "The Tragedy of 
Commonsense Morality", which is a higher level version of the
commonly referenced Tragedy of the Commons in economics. The latter is
the idea that in some situations if a resource is available to a group
of individuals then the individually rational (in the economic sense)
thing for each to do may be to use up as much as possible, thereby
depleting and despoiling the resource. One imagines, for example, shepherds
with a common pasture, where the simple way to benefit the most is to 
acquire as many sheep as possible.

This is prevented by groups of people defining systems to prevent it. 
There is no one canonical system for doing so. In the sheep example, one
can imagine a quite capitalistic answer, where everything is privatized
and future results are based on the idea of property ownership and transfer.
Another answer is to continually divide everything evenly among the 
existing population of humans. There are many other possible solutions. The
common thread is that they all provide a way to avoid the Tragedy of the
Commons by providing a way for the group of people to prioritize Us over Me. 

The Tragedy of Commonsense Morality then asks the question - what happens
when two or more groups that have so resolved the Tragedy of the Commons in
their own and different ways come into contact and have to resolve a conflict
over a new resource? Now the question is lifted to one of Us versus Them, two
or more groups. We might imagine that one of the two groups above would 
suggest having an auction to distribute the new land, and the other would
want to put it into the pool and again make sure it is equally distributed
to all. The systems that they individually use to resolve things are 
fundamentally incompatible.

This, then is what Greene wants to contemplate. When people have derived
different moral systems in their groups, how do we mediate conflicts
between those groups, especially when the group members are quite attached
to their own ways of approaching problems and likely contempuous of other
groups and the ways that they've found to handle the same problems?

A key feature of this book is that it approaches an answer not just from 
the perspective of philosophy, but from psychology and neuroscience as 
well. This is illustrated by considering the classic Trolley Problem and
its variations, not just as a thought experiment, but actually using
brain science to figure out what is going on and why people have different
reactions to the variations which all in some sense come down to one life
versus five. In some situations most people will say it is right to sacrifice
one life to save five; in others most will say the opposite. Experiments
are described to get to the root of why.

Greene makes an analogy to modern cameras, which may have an automatic point
and shoot mode and another manual mode that allows the photographer to 
adjust various parameters at will. We have the capability to make decisions
based on quick, intuitive, emotional brain function and also based on more
considered thought. Moreover, both of these are useful in different 
situations, much like the camera. And importantly the brain is acting different
physically in the two situations, meaning that we can figure out when
one mode or the other is being used. 

The indications are that when people see one side of the trolley problem that
one of these modes is being used, and when they see the other side it is
the other that is dominant. And the variations engage one side or the other
more strongly, explaining why people react so differently to them

Greene's idea is that the individual moralities of the groups of herders are
defined more by the point and shoot mode. And this was a useful evolutionary
step to allow a concept of Us over Me to arise. However he believes that this
mode is less useful, or even harmful, when considering Us and Them. The 
solution must be to enter manual mode when thinking about such conflicts
and to develop some sort of meta-morality that enables groups with 
different belief systems to mediate conflicts in spite of those different
belief systems. 

The particular meta-morality that he proposes is utilitarianism. This particular
philosophy is subject to much criticism, so some time is taken to address
the common arguments against it. In short, the idea of utilitarianism is
that the morally best thing is for the total good of society to be maximized.
The objections often take the form of pointing out that maybe this happens
if certain people are harmed. Is it okay to maximize good for everyone else
by really hurting one person - or if the total societal good is maximized
by hurting almost everyone and elevating a small group of people? 

Greene's take is that the proper measure of societal good is the total 
happiness of people living in that society, and he believes that the
type of counterexample posited above would not, in fact, in any practical
situation, result in greater total happiness. He believes that the problems
seen with utilitarianism stem from using some other measure (say, trying 
to measure economic results instead of happiness) and that they are not
likely to arise in practice when using the happiness measure.

(As an aside I would say that it is reasonable to think that there is
some sort of utility function that does well enough -
i.e. that it produces a society that, if not optimal, is at least good
enough for us to be happy with. For anyone familiar with my bent of thinking,
I believe that such a function is probably one that prevents anyone from being
all that miserable and compresses the range of individual happiness, not
necessarily trying to make it uniform, but at least fairly bounded. The
most miserable in society should not be that much less happy than the 
happiest. But this is me, not Greene.)

Now, how does this apply the the Tragedy of Commonsense Morality? Greene 
believes that the various approaches that people have developed to mediate
Me vs. Us are really meant to ensure happiness of the group. So there is
some amount of common ground between the groups. The hope is that they
can agree that maximizing overall happiness is a suitable goal and then
engage the manual mode of thinking in order to overcome the Us vs. Them 
problem. His thought experiment in this is abortion. So he examines the
various viewpoints on the issue from a variety of sides through the lens
of increasing total happiness.

Hopefully I have not grossly misstated Greene's views here. I personally
find the arguments quite compelling, and find it especially useful that
the actual biological functioning of the human brain is brought into play,
not merely abstract philosophical arguments. I may be less optimistic that
this will actually work, though. It's just unclear to me that people of the
various tribes are going to be willing to lose, or to admit that going
against their automatic setting moral sense will actually increase happiness.
I hope I'm wrong. Greene does also present some evidence that there is
a chance that this sort of thing could work and the factors that go into that.

For example, people tend to think they know more about things than they do.
This is commonly known. And they react to things based on the more automatic
systems. However, if you ask them to _explain_ how something works, this 
will confront them with their own ignorance when they are unable to do so
and the more manual systems of thought can be brought to the fore. 

This touches about the last point that I want to explore, that of _rights_. 
Greene takes a stand against rights as an absolute pillar of morality.
They function as an automatic mode reaction, allowing people to take a 
stance one way or another on an issue without entering into any manual
mode thinking. I have to say I agree here. There is still a place for rights
in his thinking, but as a short way to summarize things that have already
been thought through and decided. E.g. if we take a stand that there is a 
right against slavery, this is because we've thought through the moral 
implications and decided that it does and can not function to increase
overall societal happiness, and now casting it as a right allows us to 
concisely summarize this and get on with other things. I like this view, and
it parallels my view about what is good and bad common sense (namely, 
whether it is a distillation of real fact and evidence that has been 
simplified for everyday use and is always subject to going back to
the evidence and fact, or if it is just a simple, naive, or historical 
way of thinking about something that does not have roots in real evidence,
or at best in bad or outdated evidence). 

The fact that I've chosen to write so much about this book is evidence that
I highly recommend it. You may or may not agree with everything that Greene
presents, but it is almost certain to get you thinking. There is tribalism
all around us today, between nations, between various religious groups,
between the religious and secular, between classes. I have my doubts that
this will furnish a solution, but surely it is better to try than not
to try, and this provides a plausible place to start.





