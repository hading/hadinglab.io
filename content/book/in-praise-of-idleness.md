---
title: "In Praise of Idleness"
subtitle: ""
authors:
  - "Bertrand Russell"
date: 2023-10-22T10:45:24-04:00
tags:
  - philosophy
  - economics
categories:
draft: false
---

This is a collection of essays by philosopher Bertrand Russell. While I am not deeply steeped in philosophy, Russell is one of my favorites. While I don't always agree with him, though I do often, his views usually seem to be at least reasonable to me. And of course to the mathematically minded he is ever the discoverer of the Russell paradox, so of interest if for no other reason than that. 

I was looking for some Russell to read, and the title intrigued me. The essay of the same name hits, somewhere around a hundred years ago, the same kind of point that I and those like minded have today - if we can accomplish all the work we need in less time, which Russell thought was imminent even then, then why would we artificially require more work just to meet some arbitrary standard. To wit, if society could function perfectly well with everyone working twenty hours a week, why in the world would we insist on forty? Why not free up that time for people to do, you know, what they like?

And yet, here we are. I personally think there is every reason to believe that the forty hour week that we hold as standard here in the US and much of the west is overly much, and though I don't know what the necessary number is, it seems like it should be less. And yet we worry about technology taking away jobs even though that is only a problem if we lack the vision to simply reformulate economies in such a way that everything still gets done but less work is required and wealth is still distributed (or even better, better distributed) to meet everyone's needs. And Russell is making these basic arguments a long time ago.

I'm writing this a while after I read the book and without access to it, so I don't recall all the particulars. Looking at the table of contents online I think that some of the other essays are on similar topics. One, I believe, was how we could reimagine architecture and living spaces to support the kind of vision of society above. I'd say Russell would roughly correspond to a democratic socialist as understood today, and there are more that are about that, I think. And to his credit, he was himself of the aristocracy in England, making coming to this sort of view even more of a credit. He was quite progressive for his time.

Even though the rest may have just melded into the stew of my mind, I can always recommend Russell.