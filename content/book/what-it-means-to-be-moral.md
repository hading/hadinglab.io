---
title: "What It Means to Be Moral"
subtitle: "Why Religion is Not Necessary for Living an Ethical Life"
authors:
  - "Phil Zuckerman"
date: 2020-02-03T11:03:33-05:00
tags:
  - philosophy
  - moral philosophy
  - ethics
categories:
draft: false
---

I read this book a while ago and the details aren't fresh, so I'm not
going to put down too much here, since the ideas here interact with
other sources and I don't want to conflate too much. That shouldn't be
taken as a negative against the book - it's really good.

It's a common assertion among the religious that morality and ethics
must naturally flow from a god or other religious source. Zuckerman's
book is not only a rebuttal of this claim and a demonstration of how
perfectly natural factors may logically produce ethics, but takes a
stronger stance that morals necessarily _cannot_ be produced from a
god.

The basic philsophical argument on the second point is that if on the
one hand morality is simply a question of obedience to God then it is
fairly meaningless as God can equally well order us to love our
neighbor as ourselves as to commit genocide - and indeed the Bible
contains both (but this is more a historical than philosophical
point). Moreover these are completely arbitrary commands. On the other
hand, if God can only ask us to be moral in a way that conforms to
some outside notion of morality, then God is unnecessary in the
equation. Of course, Zuckerman states it better than this. He asserts
that theists have no real response to it; I'd be interested to know
what the attempts are.

So what then of secular morality? On a practical level, more secular
societies (here the canonical examples are places like the Nordic
countries) seem to produce greater happiness and well-being for their
citizens, less crime, and so forth. Secular people don't seem to
descend into some sort of feral, dog-eat-dog state defined by
self-interest and selfishness. And so on. You can measure a lot of
this stuff via various social sciences and when you do you just don't
see problems.

On a more theoretical level, then, where does it come from? Why
_don't_ people descend into a mire or selfishness? The short answer is
that humans have evolved as social animals. Cooperation was (and
continues to be) fundamental to their existence. Therefore it's no
grand surprise that they develop systems to enable this
cooperation. Now that is clearly a giant oversimplification, but it's
at the root of things. I have another review for a book called _Moral
Tribes_ that goes into this sort of thing more, including the
important in/out group distinction, why that might have evolved, and
what issues it causes in the modern world.


