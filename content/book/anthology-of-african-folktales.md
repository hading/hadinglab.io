---
title: "Anthology of African Folktales"
subtitle: ""
authors:
  - Emmanuel Anaman
date: 2019-02-15T13:19:02-06:00
tags: 
  - folktales
  - Africa
draft: false
---

This is another library book.

The folktales, at least as many as I read, are fine. They are primarily Ghanaian. Likely some of them will be familiar and some new.

However, I couldn't get more than about halfway through the book. The author's native language seems to be French, and he did the translation himself. The English doesn't really flow that well. Moreover, the book sorely wants for copyediting. There are a raft of issues such as spelling problems, letters jumping from one word to an adjacent one, even one place where several paragraphs were repeated verbatim after they originally appeared, and seemingly not purposefully. Footnotes to explain certain terms are indicated, but are at the end of stories and often require page turns to access, and sometimes are just missing.

One might hope that the original French edition is better in many of these areas.

