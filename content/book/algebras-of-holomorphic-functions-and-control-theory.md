---
title: "Algebras of Holomorphic Functions and Control Theory"
subtitle: ""
authors:
  - "Amol Sasane"
read_by: ~
date: 2020-08-25T14:22:02-04:00
tags:
  - mathematics
  - control theory
  - function algebras
categories:
mathjax: true
draft: false
---

I've had a first read through the book, though without trying the
exercises or assaying the technical details of the proofs. It is
pitched at an undergraduate level (though it has glimpses of things
like Banach algebras that are typically encountered later), and I
think that is about right. For my current state of mathematical
ability that is probably about right. It is also only intended to
convey a basic flavor of control theory. Since I had no real prior
knowledge of control theory, this was also of interest to me. I had a
vague sense of what it was, had heard of the subfield of $H^{\infty}$
control theory, and frankly wondered what $H^{\infty}$, a previous
acquaintence of mine, might have to do with control theory.

The book at hand only considers discrete control theory. Basically, we
have a system with an internal state and discrete time. At each time
step we send in an input and receive an output. We take the internal
state to be in an arbitrary Hilbert space and the input and output to
be complex. The next state and the output are then found by linear
operators on the current state and input. I.e. if $x$ is the state,
$u$ the input and $y$ the output, then $x(n+1) = Ax(n) + Bu(n)$ and
$y(n) = Cx(n) + Du(n)$ where $A,B,C,D$ are bounded linear operators
between the appropriate Hilbert spaces.

A first question of interest is something like this - if the sequence
of inputs is controlled somehow, is the sequence of outputs? E.g. if
$u(n)$ is in $\ell^{\infty}$ is $y(n)$ in $\ell^{\infty}$ as well? We
might then ask, even if our given system does not have this sort of
property, can we design and hook up another system (the _controller_),
combining the output of one system with the input of the other, to
achieve this sort of result. This is the stabilization problem, and
this is the kind of problem that makes this theory interesting for
engineering applications. We might then ask what happens what happens
for initial systems or controllers near to those we started with,
modelling uncertainty in our knowledge of our initial system or design
of our controller.

Now, what does all of that have to do with function algebras? On the
face of it, nothing. However, one can use something called the
$z$-transform to get there. Basically, if you have a sequence of
values $h\_n$ in a Hilbert space and you form the sum of $h\_nz^{n}$
then you start to enter that realm. If $x(0) = 0$ then the
$z$-transforms of the $u$ and $y$ sequences (which, recall, are just
complex sequences, so have $z$-transform simply a holomorphic
function) are related fairly simply by something called the _transfer
function_. And now you can make a lot of hay in function algebras.

The book at hand, being designed for the audience mentioned, doesn't
really go into the full $H^{\infty}$ generality. Instead it considers
some simpler sub-algebras of functions on the disc, namely
$R(\mathbb{D})$ (the rational functions), $A(\mathbb{D})$ (the disk
algebra of holomorphic functions with continuous extension to the
boundary), and $W^{+}(\mathbb{D})$ (the Wiener algebra, holomorphic
functions whose coeffient sequence is in $\ell^{1}$).

Much of the book is about relating properties of the transfer function
in these algebras to our control problems, which is an interesting
ride.  I look forward to going back and reading the book in more
detail and doing some of the exercises. You can also imagine from this
a taste of how the continuous theory might work - in this case time is
not discrete and the input/output/state are adjusted accordingly,
which gets us into the realm of differential equations and continuous
dynamical systems.
