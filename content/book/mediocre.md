---
title: "Mediocre"
subtitle: "The Dangerous Legacy of White Male America"
authors:
  - "Ijeoma Oluo"
read_by: ~
date: 2021-03-27T07:30:59-04:00
tags:
  - race in America
  - white privilege
  - male privilege
categories:
draft: false
---

Oluo examines white male privilege in America from the perspective of
a black woman. Despite the serious nature of the topic it's a breezy
and often funny read. We see, of course, how a country that was
largely built by everyone else pushes them aside for a narrative that
casts the white male as the truly important one. We see what happens
when others try to import their concerns and perspectives into realms
dominated by white males. And so forth. It's been a while since I read
the book, so some details have faded, but it's well worth the read, I
think especially for white males who, if they keep an open mind, will
gain some perspective that perhaps they lacked before. And I'm sure
that anyone else will spend a lot of time nodding along to things that
they've experienced.


