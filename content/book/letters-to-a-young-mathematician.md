---
title: "Letters to a Young Mathematician"
subtitle: ""
authors:
  - "Ian Steward"
read_by: ~
date: 2019-11-06T10:51:43-05:00
tags:
  - mathematics
  - mathematics education
categories:
draft: false
---

Stewart uses the form of a series of letters to a budding
mathematician (Meg) as she progresses from grade school on to
professor. He talks about the art and science of mathematics, what is
really is (in his view, which unbeknownst to me beforehand is quite
similar to my own view), how it is different than it is commonly
perceived, what a career in math really entails, and so on.

It would have been fun to have had this to read when I was taking my
own journey in this direction. I wonder if it would have warned me
off, shown and encouraged me in things that would have made that
journey more successful, or if I would have just plowed along as I
actually did. I rather suspect the last, though I think I would have
at least understood through my stubborness why that wasn't destined to
work.

So if you're interested in being a mathematician this is definitely
something to read. If you're just interested in what mathematicians
actually do (hint: it's not arithmetic on really big numbers) then
it's for you as well.
