---
title: "The Big Questions of Philosophy"
subtitle: ~
authors:
  - "David K. Johnson"
read_by: David K. Johnson
date: 2020-03-20T08:41:42-04:00
tags:
  - philosophy
  - moral philosophy
  - epistomology
  - political philosophy
categories:
  - audiobook
draft: false
---

During the course of my college career I tended to avoid "101" sorts
of courses and try to take more advanced but narrowly focused courses
to fulfill requirements. Thus, I never had something like a Philosophy
101 course. This is such a course, I think.

Johnson covers such ground as the basis and reason for philosophy,
what is knowledge and how do we know things, does God exist, does free
will exist, what is the mind and self, what basis is there for ethics
and morals and what should they be, how do we achieve meaning in life,
and why and how should we organize society.

He covers major historical arguments in these areas and more, talks
about the philosophers that have addressed a particular question, what
conclusions they have come to and why, and what objections can be
raised. As an example, in the discussion of the social contract we get
Hobbes, Locke, and Rousseau explained, compared, and contrasted.

As you might expect, such a course, even at length (this was something
in the neighborhood of 20 hours as an audiobook, I think) could hardly
be comprehensive. Johnson aims to get at some of the main points in
each area along with some of the main thinkers. In this sort of course
I think it unreasonable to expect more than that.

I liked it. It's not dry, and is peppered with pop culture sorts of
examples, although at this time some of them are a bit dated. They
clearly reflect Johnson himself, though. I think it's good for getting
an initial grounding in these sorts of things and perhaps as a
springboard for what might be interesting to look into deeper.

The course is quite particular to Western philosophy. I'm not sure
that I recall any Eastern references. I don't take this as a serious
negative.  It is simply where the focus of this body of ideas lies.

