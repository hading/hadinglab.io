---
title: "The Basic Writings of Bertrand Russell"
subtitle: ""
authors:
  - "Bertrand Russell"
date: 2023-10-22T10:45:43-04:00
tags:
  - philosophy
categories:
draft: false
---

Even less so than my other recent mention of Russell will I be able to go into detail here. That was a book of about 200 pages, this is some 700, where again I read it a while ago and don't have a copy now.

The aim is to give samples from all of Russell's philosophy, both in time and subject. So we have things like the social issues he talked about in [In Praise of Idleness]({{< ref "/book/in-praise-of-idleness" >}}). But also philosophy of: logic, mathematics, epistemology, metaphysics, morality, politics, science, etc. and etc. 

Great for getting a sense of the broad sweep of his intellectual career. Once again fascinating for how progressive he was in many ways for his time, though there is the occasional bit that doesn't read well today. I very often agree with him, and even when I don't I almost always think that his ideas are worth taking seriously - there is little of the head scratching that I think some philosophy can induce.