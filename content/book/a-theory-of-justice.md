---
title: "A Theory of Justice"
authors:
  - John Rawls
date: 2019-02-15T13:18:50-06:00
tags:
  - moral philosophy
categories:
draft: false
---

Unlike many of the books that I read, which are of a more popular bent, this is a serious tome. It is a classic in the area.

I'm still working on it, currently in section 13.
