---
title: "Storm in a Teacup"
subtitle: "The Physics of Everyday Life"
authors:
  - "Helen Czerski"
read_by: Chloe Massey
date: 2019-10-12T11:44:47-04:00
tags:
  - physics
  - popular science
categories:
  - audiobook
  - top-pick
draft: false
---

This has to be one of the best popular science books I have ever
experienced. And this coming from someone who has training in physics
and read a lot of popular science, including the greats like Asimov.

Unlike many popular science books, which deal with the large or small,
like astrophysics or quantum mechanics (and which are in their own way
fascinating, of course), Czerski deals with the stuff of everyday
life.  A cup of tea. A machine to throw boots. Trees. Making
jam. Toast falling butter side down from a table. And, given her
research one that I expect is her favorite, bubbles. This book is a
great way to answer anyone who doubts why physics should be of
interest to them.

We see how less abstruse things than QM or relativity factor into
these everyday things that we encounter. Surface tension, capillary
action, angular momentum, the ideal gas law, resonance, and so many
more all pop up to explain things that we see every day. Moreover, she
tends to isolate one such principle and then show how it crops up over
and over in different contexts to demonstrate how phenomena that at
first blush may look entirely different are in fact united by the same
physical principle. And really, isn't that the essence of physics?
Along the way she constantly urges and empirical approach - we can all
try pushing toast off a table to see if it behaves as she says, and
unlike more arcane areas of physics, most of the stuff she talks about
a reader can easily try and see for themselves. And that alone is a
valuable lesson to learn for anyone, not just in physics. You also get
at least a bit of feel for how people can take this kind of
understanding and use it to accomplish new and different things.

From an American perspective it is also quite charming that the book
is unapologetically British. The cultural differences constantly shine
through. I wouldn't necessarily say that they are played up, but
Czerski has also lived in the US, so she would certainly be conscious
of them.

Chloe Massey gives a fine reading of the text, though I was inspired
enough to also look on YouTube and watched a presentation that Czerski
gave herself over some of the same material, which was great. Czerski
is also an occasional present for BBC science shows, apparently, and I
have to think that had she read the book herself that the enthusiasm
would have come through even more.
