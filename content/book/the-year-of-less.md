---
title: "The Year of Less"
subtitle: "How I Stopped Shopping, Gave Away My Belongings, and Discovered Life is Worth More Than Anything You Can Buy in a Store"
authors:
  - "Cait Flanders"
read_by: Cait Flanders
date: 2020-03-20T08:40:43-04:00
tags:
  - memoir
  - decluttering
categories:
  - audiobook
draft: false
---

Flanders, an editor/writer/blogger having already undertaken in her
young life (I think she was in the neighborhood of 30 when she wrote
this) missions to quite alcohol, manage weight, and get out of debt
decides to cut out unnecessary shopping for a year. At the same time
she takes stock of the possessions that she already has and trims them
by something like 70% over the course of a year. She also has some
significant life events happen over the year that interact with her
quest.

Unlike some other books in this vein this is clearly a memoir - it
does not purport to teach others how to accomplish the same goals,
though that is addressed briefly at the end of the book. 
