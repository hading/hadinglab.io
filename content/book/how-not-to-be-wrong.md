---
title: "How Not to Be Wrong"
subtitle: "The Power of Mathematical Thinking"
authors:
  - "Jordan Ellenberg"
read_by: Jordan Ellenberg
date: 2019-12-04T09:01:00-05:00
tags:
  - popular science
  - mathematics
  - statistics
categories:
  - audiobook
draft: false
---

Ellenberg is a mathematician working (I think) in number theory. This
book doesn't deal with that so much. It's more along the lines of
"Innumeracy". That is, we get a lot about how to (as the title
suggests) use mathematics and statistics not to be wrong. Inevitably
along this path we get some information on how to spot when they are
being used incorrectly or deceptively as well. And there is a constant
undercurrent of, yes, mathematics is useful to the real world, along
with a recognition that mathematics as taught to the masses does not
really do a good job of revealing this.

What sorts of things are covered, then? One of my favorite pet
peeves - not everything is a line - is hit straight away. There is a
lot of discussion of statistical reasoning, how it is used and
misused, and what some of the trouble spots are. Of course we see
probability and the interplay between things being unlikely yet there
being so many things that some unlikely things are likely to happen
(and then get too much importance attached to them). I believe
"Innumeracy" had an example concerning the likelihood that you
actually have a disease given that you test positive for it (lower
than you think) - this has a corresponding example about Facebook and
terrorists. Both examples are there to show how easy it is to get
conditional probability wrong.

There is also, as should be the case in just about all of this type of
book these days, discussion of voting mathematics. Although it stops
short of Arrow's theorem, it gets most of the way there, and there are
concrete examples of not only how traditional voting can produce
results that seem wrong, but also how variations (such as ranked
choice) can do the same. Arrow's theorem basically says that you have
to pick your poison here, and while I'd prefer the poison of ranked
choice between the two. And while I like other ideas even more, it's
important to know that whatever one is picking it is mathematically 
proveable not to be perfect (i.e. such _does not exist_).

Ellenberg ends with an argument against the view that to look at
things in the ways he described, which often include recognizing and
embracing inherent uncertainty, as a sort of namby-pamby, wishy-washy
take on the world. I agree - the idea in my view is to take in truth
as closely as we can, and that to me means dealing with the fact of
uncertainty. An example that Ellenberg does not use to make it
concrete - there are statistical systems for predicting the outcomes
of (say) basketball games. We can collapse the information that they
give to just judge them on how often they predict the winner of a
game. However, that is a bad way to judge. Better would be to take a
collection of games that they say are (for example) 70-30 shots. Does
the favored team win about 70% of the time? Do the same for other
brackets. Some of them may even provide more detailed predictions. How
close do they come to those? That's a fairly trivial example, but gets
at the heart of how we should really evaluate models in a stochastic
world.




