---
title: "A Fixed Point Farrago"
subtitle: ""
authors:
  - "Joel H. Shapiro"
date: 2023-10-22T10:42:47-04:00
tags:
  - mathematics
  - analysis
  - fixed point theory
categories:
draft: false
---

I saw the MAA review of this book some time ago and very much wanted to read it. Shapiro is the author of "Composition Operators and Classical Function Theory", a book that was new during my time in grad school and which I picked up off of the new book shelf, decided to try to read through in hopes that it would illuminate Hardy spaces better for me, having previously encountered them but not really grasped them well in a class using Garnett's "Bounded Analytic Functions", and thus became sufficiently interested in composition operators to alter the direction of my attempted research.

But on to the book at hand. It's aimed at upper level undergraduates or early graduate students, making it ideal for me as I try to ramp up to reading some grad level math again. As one would guess from the name of the book, the focus is on a variety of fixed point theorems. It seems that the book grew out of a seminar on the same. 

So we get fixed point theorems such as (a sampling): 

- Newton's method
- Brouwer's fixed point theorem
- The contraction mapping theorem
- Nash equilibrium in game theory
- Markov-Kakutani

Along the way we encounter plenty of interesting stuff - Google page rank, the Banach-Tarski paradox, the invariant subspace problem, and so on, all of which have ties to fixed points. The approach is primarily analytic. For example, Brouwer is proved using approximation of continuous functions by differentiable ones rather than the sort of algebraic topological proof that one might commonly see these days. But this is no surprise since Shapiro is an analyst, and his writing here is as engaging as it was for me twenty five years ago on composition operators. 

There are exercises interspered throughout, most of which I was able to do, and I learned some analysis that I hadn't really know before, e.g. Banach means, some harmonic analysis and Haar measure and related. I knew the Banach-Tarski paradox of course, but not how one goes about proving that you can do it.

So all in all this was quite an enjoyable book to read.