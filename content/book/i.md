---
title: "i"
subtitle: "new and selected poems"
authors:
  - "Toi Derricotte"
date: 2019-08-01T22:04:03-05:00
tags:
  - poetry
categories:
draft: false
---

I am not much of a reader of poetry, and only occasionally a writer in
the limited sense that goes little beyond high school English
exercises. Nevertheless, sometimes I'll pick up a volume at the
library and give it a go. 

I hadn't any idea who Derricotte was before picking this up. I found
that I liked the poems a lot. They're very direct and personal. Maybe
the best comparison that I know (again, not my specialty) is
Plath. This is a collection that has poems spanning a large portion of
her life and heavily drawing on her family experiences, good and bad.
I'm glad I read it.
