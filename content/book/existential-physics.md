---
title: "Existential Physics"
subtitle: "A Scientist's Guide to Life's Biggest Questions"
authors:
  - "Sabine Hossenfelder"
read_by: ~
date: 2023-11-24T15:31:13-05:00
tags:
  - physics
  - philosophy
categories:
draft: false
---

The second book by Sabine Hossenfelder here. I'd say this one's theme is to question when something is scientific or, to use her term, ascientific. We look at a number of speculative or philosophical topics and see how science enters into them and what it has to say about them. This is perhaps best explicated by some examples.

A favorite is multiverses, and I think it is a good example for how the book works. There are plenty of physical theories that imply or use multiverses. Here she would say that there isn't any actually evidence that they exist; indeed, it's unclear for most of them how you would try to observe them (though for a few theories there are possibilities). So really, you're free to believe that they do or don't, but lacking a way to tell, she'd call such a belief ascientific. 

On this particular issue I'd take a slightly more moderate stance (influence by Sean Carroll) that if your physical theories, created to explain what we _can_ see in _this_ universe work to explain those things in a useful quantitative way, and then they happen to predict a multiverse beside (which, as Carroll points out is how multiverses tend to crop up - people don't invent them because they just happen to want a multiverse), then I'm fine with believing that there could be a multiverse. But I'm still not going to put much stock in it if it's not observable - in fact that again puts us back in the situation of who cares whether you believe it or not. And I might be in favor of seeing if the theory can be expressed in a way that retains its explanatory power but doesn't require anything outside of our own universe to be there.

Another example is free will. I think the short summary here would be, sure, free will can exist scientifically if you define free will in such a way that it can. Reallly what science has to say about free will (if anything) depends pretty strongly if not entirely on how you choose to define free will in the first place. So you have to nail that down before thinking about whether it is scientific or not. This I think is typically of a variety of issues, even generally in philosophy, but certainly when it comes in to contact with science. It's possible to have a lot of argument, but you may well find that if you bother to nail down exactly what you're talking about then the problems can melt away. They're more linguistic than real. 

I think that's enough to get the flavor of the book. We talk about some topic, the science around it, and perhaps get to a conclusion that science rules it out, that science _doesn't_ rule it out even if it seems unlikely, or that science doesn't really have anything to do with it.