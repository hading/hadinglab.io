---
title: "Capital and Ideology"
subtitle: ""
authors:
  - "Thomas Piketty"
read_by: ~
date: 2021-03-27T07:31:46-04:00
tags:
  - economics
  - inequality
categories:
draft: true
---

As of this writing I've only worked through the first two parts of
this book.

Piketty made a splash with his previous book, *Capital in the
Twenty-First Century*, which I've not read. The book at hand deals
again with his specialty, that is, economic inequality. Here I am
going to be able to give only the barest summary of this interesting
book.

Running at over 1000 pages, the book is a bit intimidating. I think
it's worth reading the introduction even if you're scared by the whole
book. The main point is broached - the amount of inequality we
tolerate in a society is a _choice_ that we make, not a product of
some sort of economic fundamentalism. Moreover it is a choice that
those advantaged by it should have to justify, not just at the onset
but as the society evolves.

The book starts by looking at inequality from a historical
perspective.  The concept of a ternary inequality regime plays a large
role throughout this analysis. Many historical societies had three
branches, corresponding roughly to a nobility, possibly warriors; a
clergy; and everybody else, the workers and so forth. Economically the
first two were typically quite privileged and the types of
justifications given for this are examined. Naturally, the societies
come in many variations, which are examined for their parallels and
differences.

The next part examines slave and colonial societies. Again the ternary
model is employed in this context. We see how these societies were
formed and how they were done away with and the economic implications.
In particular we see how properitarian philosophy tended strongly to
demand compensation for the privileged but not for the commoner, e.g.
slaveholders to be paid when their slaves were freed, but nothing to
be given to slaves for their years of labor. A particularly
illuminating example is Haiti, where the country was yoked with the
debt to compensate slaveowners, giving the former slaves a
double-whammy as the country to this day is affected by that decision.
Again, these considerations are examined in a wide variety of contexts
for their similarities and differences.
