---
title: "Al Franken, Giant of the Senate"
subtitle: ""
authors:
  - "Al Franken"
read_by: Al Franken
date: 2019-11-06T09:35:02-05:00
tags:
  - politics
  - comedy
categories:
  - audiobook
draft: false
---

I should start by acknowledging the later events that would prompt
Franken's resignation from the Senate and also my ambivalent feelings
about them. They _seem_ incongruous with his policy fights in favor of
women and other aspects of his life, but we are all a pile of
contradictions, and I think like many I wish that there could have
been more heard from both sides so that we could know the truth better
here.

That said, let's proceed to the actual content of the book. Franken
is, of course, a comedian/writer/satirist turned United States
senator. If you understand the former, then you'll understand that
he's not calling himself a Giant of the Senate. It's a joke, which in
some ways is a recurring theme of the book. He does name some he
regards as actual giants of the Senate, though, like Ted Kennedy or
his personal hero, fellow Minnesotan Paul Wellstone, whose particular
influence Franken mentions explicitly and often.

The book starts by recalling Franken's younger years and his time in
comedy, at Saturday Night Live, and so on. We see some things that
affect his outlook, including his writing partner's and wife's
struggles with addiction.  This takes a few chapters, but the focus
here is on the political part of his life.  We get some background of
his political books debunking and satirizing the right-wing politics
and media machine of the day. Now, those books are unquestionably
comedic as well as political, and more so than the one at hand. This
is not to say that humor is not used to good effect in this one, but I
think it is more sober than some of the others.

We pretty quickly get to the death of Paul Wellstone (whose saying
"Politics is about improving people's lives" is a mantra for Franken)
and the subsequent ascension of Norm Coleman to that Minnesota senate
seat, and how Franken's thought that someone has to beat that guy
gradually comes to be _I'm_ going to beat that guy.

Now, for reasons that I find odd, many people find the prospect of a
comedian getting into politics odd. I mean, a big part of comedy is
finding inconsistencies between reality and perception, and cutting
through bull. Exactly the kind of thing it would be useful to have
represented in politics instead of a constant diet of business and
law to the exclusion of all else. But I digress. The fact is that 
this was a tremendous hurdle to Franken. 

We get a sometimes day by day, blow by blow account of exploring
running, working to win the primary, dealing with potential problems
even after winning the primary, then the race with Norm Coleman,
counted as lost on election night but won on the recount. We get a
sense of the grind of traveling the state, interacting with
constituents from all walks of life, daily polls and fires that need
to be put out. And lots about fund raising, which no one wants to do
but everyone has to do. If you want to know about the nitty gritty
of this kind of political campaign, this is the place to go.

Franken's entry into the Senate was delayed by about six months by the
recount. We then get a similarly detailed look into how the Senate
really works, how Franken gets his feet under him there, and why
certain things are necessary to get things done that the more partisan
of us don't like to see. It's instructive to be reminded that he may
need to get along and work with other senators that I, for example,
might find somewhat odious because they agree on enough to get
_something_ positive done. Except Ted Cruz. As Franken says, he likes
Ted Cruz more than most of the other senators (of either party). And
he hates Ted Cruz.

There is a lot more to be said than I have time to say. One might say
that one of the main themes is simply that if one is in politics, and
one has the vision of Paul Wellstone to use it to improve people's
lives, then with hard work, persistence, and dedication this is
possible, though almost certainly more slowly, less completely, and
with more resistance and opposition than one would hope. But it can be
done, and should be fought for. You'll also get the sense that truth
is something worth fighting for.

