---
title: "What If"
subtitle: "Serious Scientific Answers to Absurd Hypothetical Questions"
authors:
  - "Randall Munrow"
read_by: Wil Wheaton
date: 2019-10-12T11:44:27-04:00
tags:
  - popular science
categories:
  - audiobook
draft: false
---

Randall Munroe is better know as the creator of the xkcd comic. He is
asked many questions via his website and this book is the result of
his trying to answer some of the more interesting (although always
rather absurd, as the title indicates) ones by actual scientific
reasoning or simulation, perhaps also extending or varying it to take
it to an extreme or in a different direction.

For example, he handles pressing questions like what would happen if a
baseball were pitched in a game at a significant fraction (I don't
recall exactly) of the speed of light? (Short answer - this would not
really be good for anyone.) Or, can you cook a steak by dropping it
from high enough in the atmosphere? (Short answer - no.) What would
happen if everyone pointed a laser pointer at the moon at the same
time? What would be the best strategy for two immortals on the Earth
to find each other?

As you might expect, the answer itself is rarely the most interesting
part, but rather how one gets there using real science. It could serve
as inspiration for thinking carefully and precisely about anything by
illustrating how to do it even about kind of frivolous things. If
you're conducting a job interview it might give you inspiration about
the kind of question you could ask just to see how someone reasons
about a question that they probably haven't thought about before.

Not least, it was fun to have Wil Wheaton reading. This is exactly the
kind of book where you say to yourself, yeah, it makes sense that Wil
Wheaton is the one doing this.

