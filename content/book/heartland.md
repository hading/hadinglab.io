---
title: "Heartland"
subtitle: "A Memoir of Working Hard and Being Broke in the Richest Country on Earth"
authors:
  - "Sarah Smarsh"
read_by: Sarah Smarsh
date: 2020-01-15T09:09:09-05:00
tags:
  - memoir
  - inequality
  - social justice
  - Kansas
categories:
  - audiobook
  - top pick
draft: false
---

Sarah Smarsh grew up in rural Kansas, to the west of Wichita. This is
the story of her and the family that she knew there, framed as a tale
told to her never born daughter, the spectre of whose possibility
always hung over her life.

The people in her family did things like farming, carpentry,
waitressing, being a real estate agent, and so forth. And Smarsh did
some of those as well on her way to becoming a professor of
journalism. Women typically got married and had children young, not
necessarily in that order - Smarsh early on decided that she didn't
want that to be her fate and worked against it, not wanting to see her
daughter born into the same cycle of poverty that she was.

The point, though, is that this poverty is systemic rather than
self-inflicted. Her family works hard, both men and women, but the
institutions of society are constructed not to help but to hinder them
from getting ahead. Moreover, the value system that is inculcated in
them makes them believe that they, and not society as a whole, are
responsible for their position. Smarsh regulary explores how various
institutions and policies work against these people and for those
already better off, and how they may nevertheless support those very
policies. In some sense this book puts a very personal face on the
issues in Thomas Frank's "What's the Matter with Kansas". While I
think the blame is fairly placed more heavily on certain areas of the
political spectrum, no side is left unchallenged.

Though the family has many struggles - financial, physical, addiction,
relationships and family - they also experience joys and happiness.
They take pride in their work and do it as well as they can, even if
they are not sufficiently rewarded for it.

As Smarsh remarks near the end, the daughter she has been speaking to
will never be born - not necessarily because she won't ever have a
daughter, but because such a daughter would be born into radically
different circumstances given the progression of her life, not to a
young mother trapped in a cycle of poverty.

Smarsh reads the book herself. The delivery is somewhat flat, but I
don't say that as a negative. It conveys the character and characters
of the book much better than an actor might have if they decided to do
it in a more dramatic fashion. The people in the book are not simple
or straightforward in who they areq, but their manners are fairly plain
and down to earth.


