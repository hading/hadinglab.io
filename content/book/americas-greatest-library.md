---
title: "America's Greatest Library"
subtitle: "An Illustrated History of the Library of Congress"
authors:
  - "John Y. Cole"
date: 2019-02-21T07:08:14-06:00
tags:
  - libraries
  - history
categories:
draft: false
---

There's not all that much to say about this one. It covers the history of
Library of Congress from inception to present. There are a lot of pictures.
There isn't much of an effort to weave a narrative through the history
or to dig very deep; most of the book's text consists of paragraph long
notes talking about important days in the library's history. There is
also an appendix about the Librarians of Congress.
