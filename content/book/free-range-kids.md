---
title: "Free Range Kids"
authors:
  - Lenore Skenazy
date: 2019-02-15T13:18:33-06:00
tags:
  - parenting
categories:
draft: false
---

A fun book about a movement to which I am sympathetic. The idea is
that we overworry about a lot of things with respect to children
nowdays in spite of the fact that statistically we should not, and
that perhaps it would be better for the children if we did not. Crime
rates have been dropping for around 25 years, and the benefits of
allowing children some extra freedom and ability to do things and
explore on their own without having parents hovering about (benefits
accruing to both the children and parents, no less) far exceed the
small potential of harm that comes of doing that.

This was thrust into the spotlight when the author, living in New
York, allowed her nine or ten year old son to take the subway
alone. In circles this action earned her the title of America's worst
mom. The idea of the book is to explain why this might not be the
case - indeed, why there are good reasons to allow children freedom
and responsibility earlier than we typically do today - not the least
of which is that in the very recent past (say in my generation or the
one after), they had that freedom without serious problems.

A common example is walking to school. I took a bus to school, but
that was mostly because of the distance involved, even in elementary
school. However, most of the kids in my school, those who lived
directly in the subdivision containing it, simply walked. Even in the
early grades. Now we see kids being driven to and being picked up from
school, even right around us, just two blocks from a school. Why?
That's the kind of thing that this book is trying to address. I think
we can generalize the answers (particular that of manufactured fear)
to apply more generally to our society than just to our treatment of
children.

I'm not saying that one needs to be sympathetic to every suggestion
presented or that one needs to go as far as Skenazy has done. However,
the ideas definitely merit consideration, and I am in agreement that
we have been conditioned to worry where we need not and that we might
be better off losing some of that worry, teaching children well, and
then trusting them.
