---
title: "The Inner Level"
subtitle: "How More Equal Societies Reduce Stress, Restore Sanity and Improve Everyone's Well-being"
authors:
  - "Richard Wilkinson"
  - "Kate Pickett"
date: 2019-06-05T09:09:31-05:00
tags:
  - economics
  - sociology
  - social justice
  - economic justice
  - inequality
categories:
  - top pick
draft: false
---

I had much anticipated reading this after I found it in the library,
and was not disappointed. Copious evidence is collected from numerous
studies on the effects of inequality and presented in a single volume.
There are studies quoted that compare different societies (most, as I
recall, are of this kind), but also some that examine effects in the
same society as inequality waxes and wanes. I wish that I had had the
time to write this closer to reading the book, but I'll do my best to
do it justice regardless.

To no one's surprise, the evidence strongly supports the notion that
people are happier in societies where economic conditions are more 
equal. Moreover, the unhappiness in more unequal societies lies not
only among those less well off, but pervades the entire society possibly
excepting only those at the very, very top. 

Why is this? A theme that runs through the book is human nature as a
social animal, and the isolation that accrues as this species reacts
to unequal conditions. Or, to state another way, greater equality
fosters better social interaction which produces greater human
happiness. Contrast with certain strains of economic thought which
assert (but in my opinion do not prove, and may not even attempt to
prove) that maximization of certain economic benchmarks is the key to
a happier society. There is a lot of material here to counteract that
viewpoint (roughly 500 references, mostly to studies on inequality).

The book hits such diverse points as the myth of meritocracy,
consumerism, issues around sustainability, and class differences,
ultimately connecting them all back (with evidence, recall) to
inequality. Moreover there are concrete suggestions as to how we might
strive to create a once again more equal society, though no illusion
that accomplishing this will be easy. There are also pointers to
organizations working on these problems.

An interesting point to me was that anthropological evidence strongly
suggests that early hunter-gatherer societies were quite
egalitarian. (It's also worth noting that in such societies the daily
amount of work that needed to be done to survive was actually quite
low - much lower than the eight or so that we in America think of as
normal for us.) The advent of agriculture was an important event in
disrupting this. (I'm not going to dismiss agriculture's importance,
obviously. But it is worth thinking about why it caused equality to
break down and how it, and other societal functions that we deem
necessary, might function differently to support lowered inequality.)

A bit of quotation from the summarizing chapter may be helpful:

> Let us now summarize the four key improvements in the quality of
> life which can take us towards a more fulfuilling and sustainable
> way of life.  First, through greater equality, we gain a world where
> status matters less, where the awkward divisions of class being to
> heal, where social anxieties are less inhibiting of social
> interaction and people are less plauged by issues of confidence,
> self-doubt and low self-esteem. This would, in turn, reduce our need
> for the drink and drugs we so often use to cope with anxiety and
> ease social contact.  There would be less need for narcissistic
> self-presentation, less need to overspend for the sake of
> appearances. In short, we move towards a more relaxed social life,
> with strong communities, in which it is easier to enjoy the
> pleasures of friendship and conviviality and gain a society better
> able to meet our basic social needs

The summary includes three other similar statements: using
productivity increases to increase leisure and decrease the amount of
work each person needs to do, making work a better experience by
increasing the input of workers into the shape of their work (as an
aside, reducing or eliminating the relatively recent notion that
business is obligated exclusively to its owners and not to its
employees, customers, or society at large), and enjoying the physical,
mental and social benefits of living in a more equal society.

It would be hard for me to recommend this too highly.
