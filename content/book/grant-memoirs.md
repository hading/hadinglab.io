---
title: "Grant"
subtitle: "Memoirs and Selected Letters"
authors:
  - "Ulysses S. Grant"
read_by: ~
date: 2020-08-25T14:21:42-04:00
tags:
  - memoir
  - Civil War
  - history
categories:
draft: false
---

I'm not all the way through this yet, but I'm going to go ahead and
write anyway, as I doubt that further reading is going to change what
I have to say all that much.

We know U.S. Grant primarily as a general during the Civil War and
then later as president. His memoirs focus primarily on the campaigns
he was in during the Civil War, though there is some material about
his early life and the Mexican-American war. The campaigns are
recalled in detail, often day by day, together with logistical,
strategic, tactical, political, and even psychological considerations.

Grant impresses one as someone who is truly smart, not in that they
try to convince you of that fact, but rather that they have the kind
of humanity, humility, and honesty that you would expect to find of
such a person.

Indeed, to me that most interesting parts of the book are not those
that recount the particulars of military history, but rather the
occasional tangent from that or step back to assess things from a
broader perspective. Grant now and then drops some fine humor, such as
the tale of his negotiating for a horse when he was young, or
observing a "mutiny" on a ship. Often the humor is aimed at himself
rather than others, as much great humor is.

He is also willing to give candid assessment of his own general and
the enemies. Again, he is not spared here. He notes something that has
always struck me, that there is some mythology risen around the
Confederate generals that does not seem to extend to those of the
Union, who seem to remain rather anonymous, which seems inexplicable
since the Union won the war.

You can see some of the innovations that Grant made or incepted. It
seems that some of the things attributed to Sherman have their genesis
in Grant's philosophy, though perhaps not taken as far there. The book
seems to reinforce the old adage that good generals study strategy and
great ones logistics. You also get a sense of his compassion for the
soldiers, both in his and his enemies command.

It is also interesting to see how he conceived the United States in
general, and how that relates to the differing views people have today.
As an example I take some quotes from Chapter XVI, which also serve another
purpose:

> The framers were wise *in their generation* (my emphasis) and wanted
> to do the very best possible to secure their own liberty and
> independence, and that also of their descendants to the latest days.
> It is preposterous to suppose that the people of one generation can
> lay down the best and only rules of government for all who are to 
> come after them, and under unforeseen contingencies. 

(He then lists some changes since the times of the Founding Fathers.)

> We could not and ought not to be rigidly bound by the rules laid
> down under circumstances so different for emergencies so utterly
> unanticipated.  The fathers themselves would have been the first to
> declare that their prerogatives were not irrevocable.

Later in the chapter he talks about Southern elites demonizing
Northerners and setting poor white vs. slave black as a means of
maintaining their power, even though the interests of the latter two
clearly coincide in the main, much more so than with the elites.
Shades of Lyndon Johnson's famous statement on the matter. Moreover, a
government was only acceptable if it reinforced their views on the
matter.

> The fact is, the Southern slave-owners believed that, in some way,
> the ownership of slaves conferred a sort of patent of nobility - a
> right to govern independent of the interest or wishes of those who
> did not hold such property. They convinced themselves, first, of the
> divine origin of the institution and, next, that that particular
> institution was not safe in the hands of any body of legislators but
> themselves.

Shades of the economic elites of our day, no? I wanted to include some
samples also to illustrate that the writing is clear and direct but
also quite literary. In short, while I sometimes get a bit bogged down
by the copious details of the military campaigns, the prose is a joy
to read.

Grant is sometimes given short shrift, partially because of the
corruption of his administration (though few believe he himself) as
president and the aforementioned undeserved hagiography of those that
Grant actually defeated in battle.

Now this is a memoir, though one reputedly checked well for facts, so
that should always be taken into consideration. It is Grant's own view
of events, and though to me he seems to be honest in presenting things
as objectively as he can, there is no question that there is room for
subjectivity to creep in. Nevertheless I think this is well worth the
lengthy read (it spans well over a thousand pages in the edition that
I have) to get his viewpoint on the war and the continuing experiment
that is the United States.
