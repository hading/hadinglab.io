---
title: "People, Power, and Profits"
subtitle: "Progressive Capitalism For An Age of Discontent"
authors:
  - "Joseph Stiglitz"
date: 2019-08-01T22:16:58-05:00
tags:
  - economics
  - inequality
categories:
draft: false
---

Stiglitz is a Nobel Prize winning economist who needs little
introduction. This book covers (and it's been a little while between
reading and writing this, so please forgive if details are murky or if
I get things a bit off) inequality and the problems of the current
economic system in America.

A distinction is made between economic activity that actually
generates wealth and that that just moves wealth around, and actually
decreases the total generation of wealth (as an opportunity cost of
not engaging in the first type of activity, for example).

Useful economic activity is exemplified by the like of basic
scientific research. Such spending is an investment that has
historically paid off handsomely.

Harmful economic activity is exemplified by the likes of rent seeking
and market power. Rent seeking is just what it sounds like, making
money simply by _owning_ something and not providing new production.
Market power is essentially the ability of large companies who are
already dominant in a market to prevent competitors from entering and
thus to extract greater profits than a naive free market economics
would suggest they should be able to do.

I can't provide a complete run-down of the book here, but it is what I
like to describe as reality based economics rather than faith based
economics, which is to say that it treats it as a science based on
observation rather than mathematics. There is not only an analysis of
the problems that exist but also some prescription for how we might
obtain a more equitable and overall better economy, dealing with such
things as rent seeking, market power, and domination of the political
landscape by the economically powerful.
