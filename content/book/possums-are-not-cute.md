---
title: "Possums Are Not Cute"
subtitle: "And Other Myths about Nature's Most Misunderstood Critter"
authors:
  - "Ally Burguieres"
read_by: ~
date: 2023-11-24T14:52:29-05:00
tags:
  - possums
  - animals
categories:
draft: false
---

There's not too much to say here. It's a small, short book with a lot of photos of possums and facts about them. Some of the notable ones:

- Possums _are_ cute
- Possums are not really suitable for pets
- Possums do good things, not the least of which is potentially eating thousands of ticks per week
- When possums play possum it's an involunatary reaction, not a conscious strategy

And so on.