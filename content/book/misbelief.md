---
title: "Misbelief"
subtitle: "What Makes Rational People Believe Irrational Things"
authors:
  - "Dan Ariely"
read_by: ~
date: 2023-12-08T10:19:33-05:00
tags:
  - psychology
categories:
draft: false
---

Ariely is a researcher who studies irrationality and related things. Misbelief in this book is a less loaded synonym for conspiracy theories. Ariely was personally pulled into various COVID-19 misbelief. He was a government advisor trying to help, for example, increase the amount of vaccination against COVID. This led to his being included as an evil actor in various misbeliefs about the reaction to COVID. Thus he became personally interested in how otherwise seemingly reasonable and intelligent people fall down the hole (or funnel, to use his term) of this misbelief. In the process he engaged with some of the very people who saw him as part of this conspiracy.

The funnel is a four part process involving emotional, cognitive, personality, and social factors. 

For example, he tells of a woman whose son forgot or lost his mask one day during COVID. The school's reaction to this caused in her a very emotional experience which started her down the road of COVID denial. With that precipitation she probably would not have gone that route. In general, some sort of stressor can start a person down this path as it impedes our normal faculties. Another emotional factor is having concrete actors behind bad things rather than just a sort of random confluence of the universe.

The cognitive factors have a lot to do with confirmation bias and seeking information to strengthen and maintain current beliefs. We're not very good at evaluating evidence objectively.

Personality traits may involve things like how prone one is to falsely recall things or impose patterns on the world where none really exist. 

The social factors seem dominated by ones old social group becoming more distant from an individual as misinformation takes hold while the misinformed group is quite welcoming.

All of this is presented with a lot of examples of psychological research as well as ideas for how one might help someone who is falling down the funnel. This seems mostly focused around continuing to engage them, not ridiculing them, and thus trying to keep one of their feet in the real world. Ostracizing them is likely to accelerate their fall. I have to admit, I don't know if I could really manage to do these things.

There are also stories of the various people whom he engaged who believe he is in these conspiracies. Interestingly, at least one of them came to the point that he no longer belived that Ariely was involved in a conspiracy, but said that he couldn't admit that publically, reinforcing the importance of the social aspect of misinformation. Others, even after perfectly reasonable interactions with him, continued to believe. 

So, an interesting book, but one whose practical advice might be difficult for me personally to implement. Others might be more successful with it. Even if it won't make you engage with others, it's still relevant because it may make you examine more carefully what you believe and why and if you are falling down the funnel in any ways without noticing.