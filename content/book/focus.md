---
title: "Focus"
subtitle: "The Hidden Driver of Excellence"
authors:
  - "Daniel Goleman"
read_by: Daniel Goleman
date: 2019-12-04T09:01:11-05:00
tags:
  - psychology
  - neuroscience
categories:
  - audiobook
draft: false
---

There's little like listening to an audiobook about focus whilst
commuting to highlight how poor your focus for listening to an
audiobook while commuting is. So please forgive any mistakes,
oversimplifications, or other problems with this review, which will be
brief.


Goleman is a psychologist and journalist famous for writing the book
"Emotional Intelligence". This book takes on the role of mental focus.

I would say there are two main axes in the discussion. The first
divides focus into inward/outward/other. That is, attention to one's
internal state, the state of the outside world, and the state of other
people. The other axis would be lower/higher. That is, one's instinct
versus one's conscious thought.

For us to function at the highest level we need to be able to apply
and integrate focus on all of these levels. The book contains many
examples of the famous and how they interact with these levels of
focus.  There is discussion of what can sap focus, and some practical
advice (which, alas, I was not sufficiently focused to recall now) on
ways to improve our focus.

