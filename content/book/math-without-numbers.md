---
title: "Math Without Numbers"
subtitle: ""
authors:
  - Milo Beckman
read_by: ~
date: 2023-11-24T15:01:08-05:00
tags:
  - mathmatics
categories:
draft: false
---

A popular account, without much if anything in the way of formulas or anything formal, covering topology, analysis, and algebra. I'm not really it's target audience, though I do like to read this sort of account to pick up ideas about explaining math to those casually interested. 

There are plenty of nice pictures, and a decent number of interesting results. For example we see diagonal arguments, simple intuitive topology, some indications about modelling, the lack of a non-vanishing vector field on a 2-sphere, etc., all exposited in a much more engaging way than I did just now. I'm not too likely to return to it, but the lay reader who is interested in math would likely find it a nice read.