---
title: "What Money Can't Buy"
subtitle: the Moral Limits of Markets
authors:
  - Michael J. Sandel
date: 2019-02-15T13:17:59-06:00
tags:
  - economics
  - moral philosophy
draft: false
---

The basic thrust of this book is suggested in the title. Pure economic
factors don't seem to produce a society that meets the criteria that
we think of as defining morality. Sandel examines this. I'd like to
discuss my viewpoint on the general question first, then go into some
things that Sandel brings up that I found interesting, perhaps with
some illustrative examples.

Implicit in the desire to have this discussion is the idea that morals
are independly definable from market economics. I think this obvious,
but there is a school of economics that would deny that this is the
case, but rather that market outcomes defined in a certain way
actually define morality. I find this absurd, but you can find people
who believe it.  To me this is symptomatic of a desire to define an
axiomatic system and derive consequences from it without ever
referencing the actual world, then to complain about how the world is
when it looks different than the theory's predictions. A physical
analogy would be for me to decide that I'm going to take Newton's
mechanics as a definitive description of the world. Then when we
observe things about the universe that don't fit these laws, instead
of inventing relatvity and quantum mechanics we rage against the
universe and complain that it should act more like Newton's theory
says.

If from the above you glean that I am of the opinion that economics,
as much as physics, needs to be empirical and to constantly match up
real world observations with theory and that theory is what needs to
give way when mismatches are found, then you are correct. And if you
agree then this book might be of interest to get at a little bit how
markets themselves are not sufficient to provide what we commonly
think of as moral.

Two ideas in the book particularly connected with me, those of
_fairness_ and _corruption_. Sandel contends that these are not well
handled by market models. So what do we make of them?

_Corruption_, in Sandel's sense, is not (necessarily) what we commonly
think of. It has a more technical meaning, which I take roughly as
what happens when putting a previously non-market good into a market
fundamentally changes it. Classical economic theory has no way to
account for this, and some might even deny that it happens. Sandel,
and I, think that it can. An example of what he is talking about ,
found here and in other sources, comes from the world of daycare.

A daycare would occasionally have parents come late to pick up their
children. Of course, this is an inconvenience for the daycare
workers. To attempt to reduce this, the daycare instituted a charge to
parents when they were late to pickup. The result of this was that
late pickups became more common. As Sandel points out, this was the
opposite of what one would expect from a purely economic
perspective. Surely you should be less likely to be late if it costs
you some money to do so.

The resolution of this dilemma according to Sandel is corruption,
i.e. the good changed in some way when market forces were
introduced. Without payment we had a social contract. People would be
late only if it were truly unavoidable, out of courtesy or the desire
not to be jerkish. Formalizing the system to include payment allowed
them to look at it as a contractual and acceptable action. They felt
like they would no longer need to feel guilt about the action.

_Fairness_, as I took it, has to do with the discrepancy between
willingness to take an economic action and ability to take that
action. A good example here is from the world of sports, where you may
see the most rabid of fans in the cheap, undesirable seats while much
more casual fans are sitting in the prime sections. (No one is
suggesting that it is universal, but it commonly happens.) If their
economic situations were equal, it is likely that the hard core fans
would be willing to pay more for the good seats, but if they are less
well off, they lack the ability if not the desire to do so.

This naturally leads into all kinds of philosophical questions about
how to construct a just and moral society. I don't want to pursue all
of that here. The relevant part to me is that the market based schemes
don't seem to produce a result that is consistent with intuitive ideas
of what is fair in this situation, and as I've observed, merely
stating that this is what markets produce hence it is the optimal
outcome rings hollow to me, like we have no ability to make moral
judgements outside of the purview of markets and economics.

These examples are enough to give the general sense of the book. There
are many more, involving such diverse areas as insurance, medical
care, environmental concern, and advertising. Commonly economic and
more philosophical analyses are contrasted. For the audience of this
book, the economic analysis will typically have something missing that
we can recover or at least sense when expanding our viewpoint beyond
pure economics.

Lest one think that I have no regard for economics, let me state that
this is not true. However, I feel that it is at its best when it is
firstly being done as an empirical science, as I suggested earlier,
and not just mathematics. And secondly if it is taken as a tool to try
to solve issues suggested in other ways (e.g. our moral sense) rather
than as a definer of that sense. I think it can work in that way,
although our attempts to use it must always themselves remain the
subject of observation and judgement, as the daycare example shows.

I think that this book is a good introduction to these issues and a
good compendium of examples to consider when thinking about them. At
some point I did feel like the same points were being made over and
over with only minor changes, but that is not a big criticism, and the
ideas would come across as well if a reader were simply to skip over
some of the examples.
