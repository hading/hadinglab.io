---
title: "When Einstein Walked With Gödel"
subtitle: "Excursions to the Edge of Thought"
authors: 
  - "Jim Holt"
read_by: David Stifel
date: 2019-12-04T09:00:35-05:00
tags:
  - popular science
  - physics
  - mathematics
  - philosophy
categories:
  - audiobook
draft: false
---

This book treads many of the common staples of popular science. There
are chapters on cosmology, quantum mechanics, and relativity. There
are chapters on some of the great problems of mathematics. There is
some amount of neuroscience. There is philosophy, especially (but not
exclusively) as it relates to mathematics and physics. There is also a
lot of delving into the personalities behind all of this. The human
element of participating in these areas is well represented.

To me, unsurprisingly, the most interesting parts were the
mathematical. I think the book is bold here - we get things like
category theory, the Riemann hypothesis, non-standard analysis, the
Langlands program, fractals, and more. I know too much about
mathematics to say how well this is explained for a popular audience,
but I appreciate the attempt to explain things that lie at this level
and maybe have absorbed some ways to do so myself. At some points I'm
dubious, though.  I didn't really know much about the Langlands
program beyond a fairly fuzzy idea what is is, and I don't feel like I
really know more now. I'd be curious what someone who doesn't know
mathematics would make of these sections.

The rest is interesting too. Much of the physics is well-trodden, but
it's always of interest to me to hit the most modern things which are
new since I studied it, like the implications of dark energy and the
like for the end state of the universe.

The book seems to be a collection of essays and as a result there is
some duplication, though this is not excessive. 
