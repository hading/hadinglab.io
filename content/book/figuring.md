---
title: "Figuring"
subtitle: ""
authors:
  - "Maria Popova"
date: 2019-04-30T09:24:18-05:00
tags:
  - biography
  - history
  - feminism
categories:
draft: false
---

This is an interesting book that is quite different than what
I normally read and that frankly I struggle to know how to
describe properly.

It traces the lives, professional and personal, of some significant
figures. Center stage are Maria Mitchell, Margaret Fuller, Emily
Dickinson, and Rachel Carson. (Popova also includes Johannes Kepler
and Harriet Hosmer in her list of main figures in the bibliography,
but to my mind they are not quite as central.) Along the way we 
encounter the people who were important in their lives.

The presentation is not really what one would expect as a biography
or history. It's more like a stream of consciousness, although I'm 
not sure that really describes it well either. Extended tangents are
readily taken and merged back. Events are marked in time, but the
flow of time in the narrative is decidedly non-linear and flexible,
and so in some sense harder to follow if one wants to pin down things
in a linear fashion. 

This should not be taken as a drawback unless one wants that sort of
rigid presentation. Otherwise it is utterly fascinating to read, 
especially for one like me who had little knowledge of these people.
Especially valuable is knowing the kinds of struggles that they
encountered solely because they were women.
