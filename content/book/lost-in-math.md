---
title: "Lost in Math"
subtitle: "How Beauty Leads Physics Astray"
authors:
  - "Sabine Hossenfelder"
read_by: ~
date: 2023-11-24T15:09:23-05:00
tags:
  - physics
  - mathematics
  - philosophy
categories:
draft: false
---

Hossenfelder, who also wrote the next book I'll write up, is a physicist who is a contrarian in the best way. She's also hilarious in what I think is probably a very German way, which also comes across in her numerous YouTube videos about physics and such.

This book focuses on, as suggested by the title, how (in her opinion) physics, or more properly perhaps we should say physicists, is led astray by a focus on trying to find mathematically beautiful theories.

Not that these are bad in and of themselves. Indeed, given a beautiful theory that describes the physical world and one less so, we're going to pick the former. The problem comes when the beauty, rather than description of the world, becomes the object of attention. 

It is the case that we have something, the Standard Model, that describes the world (and perhaps here we should be specific and say the world of particle physics) really well. But in some ways it is regarded as lacking in an aesthetic sense. There are a lot of constants that have to be measured and that don't have further explanation. And the suchlike.

So many physicists search for simpler, more aesthetically pleasing models of the world that will do things like explain these constants. However, in some sense, these simply are not necessary, in the sense that they don't explain anything that the Standard Model doesn't. And in case after case they predict something that the Standard Model doesn't and when experimenters go looking, they don't find it. 

Hossenfelder's opinion is that basically that this is all kind of a waste - maybe you don't like these things about the Standard Model, but it works, so get over it. Maybe that's just the way nature _is_. Honestly I don't think she's quite saying that, but rather that _until_ there is reason to doubt aspects of the Standard Model, something observational that is a problem, it doesn't seem like there is that much reason to expend the kind of effort that the community has trying to replace it. And I think that indeed she probably believes that at some point it _will_ be replaced, but that observation, not some desire to fix up some perceived klunkiness, should be the driving factor.

I'm largely inclined to agree here. It doesn't seem to me that the universe is obligated to admit a simple explanation or description for itself. 

An acknowleged critique is that the kind of experiments that are necessary to progress in this area are sufficiently expensive that you can't just do them, see what comes out, and then hope for something new to explain. You really need to have some idea, via an alternate theory, of what you're trying to test for, and in that case you need to have some idea what theories deserve testing, and for that you need to have decision criteria, for which beauty is not an unreasonable component. A counter criticism is that in some cases, physicists don't even seem to care (and will say this explicitly) that it is difficult or impossible to make their theories make contact with observation - the beauty alone is enough to recommend them.

Well, as you'd expect Hossenfelder (and I) don't really want any of that. I'm very much in the camp that if you're not explaining the observable world then what are you doing? Well, maybe you're doing mathematics, and that's fine. Obviously I don't have any problem with that. But mathematics in my view only has anything to do with reality when you show it has something to do with reality, and physics should be about reality. The book is good if you want to read an account of a physicist who wants to keep physics grounded.