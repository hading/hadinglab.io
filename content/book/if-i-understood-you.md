---
title: "If I Understood You, Would I Have This Look On My Face?"
subtitle: "My Adventures in the Art and Science of Relating and Communicating"
authors:
  - "Alan Alda"
read_by: Alan Alda
date: 2020-01-15T09:10:05-05:00 
tags:
  - communication
  - science
categories:
  - audiobook
draft: false
---


Alda talks about communication, especially in but not limited to the
communication of science. Of particular interest is his connection of
exercises in acting and improvisation (in the acting context) to
improved communication. There is an emphasis on the responsibility of
the communicator to read and react to the audience in order to improve
communication. Alda has a long time interest in science and his
passion for trying to teach actual scientists to communicate what they
do and why it is important is evident.


