---
title: "Arthur C. Clarke"
subtitle: ""
authors:
  - "Gary Westfahl"
date: 2019-05-19T09:14:08-05:00
tags:
  - literary criticism
  - Arthur C. Clark
categories:
draft: false
---

This is relatively recent literary criticism around Arthur C. Clarke.
I'm not really that familiar with older criticism with which Westfahl
wishes to take issue. I got the book because Clarke was one of the
influences of my youth, although even then I was more of an Asimov
fan.  But I liked Clarke a lot too. 

Perhaps the best thing that the book did for me was to inform me that
a story that I had read in high school and had always wanted to find
again was in fact written by Clarke, not Silverberg or Niven as I had
thought. The story in question is apparently named "Superiority", and
is about a conflict between two alien cultures, one of which keeps
producing more and more sophisticated, but unreliable, weapons, and
the other just keeps cranking out the same old workable stuff. The
result is predictable.

Anyway, Westfahl seems to take issue especially with two points from
earlier criticism. One is that Clarke has a relatively optimistic
outlook on space travel. Westfahl contends that rather Clarke saw 
humanity as possibly able to colonize the solar system, but unlikely 
to make more progress than that at least for considerable time.

The other is that Clarke's characterization is particularly lacking.
Instead, Westfahl finds it better than that, but maybe somewhat
disguised by the fact that the people Clarke is characterizing tend to
be work-focused loners who don't want to interact that much with
society, friends, or family. I don't have a strong feeling about this,
but it does seem a reasonable argument. Even so, I'm not sure anyone
is going to argue that this is Clarke's main strength or goal even,
but perhaps it does make him look a little bit better.

Really, though, reading this book for me was primarily a trip down
memory lane thinking about some of the stories that I read so many
years ago, some of which I've always remembered and some that were
long since forgotten.
