---
title: "The Royal Society"
subtitle: "& The Invention of Modern Science"
authors:
  - "Adrian Tinniswood"
date: 2019-08-01T21:59:45-05:00
tags: 
  - history of science
categories:
draft: false
---

This is a brief history of the Royal Society of London, the first
modern scientific society. The focus is mainly on the early days of
the society and the characters inhabiting it at that time.
