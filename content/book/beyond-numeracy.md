---
title: "Beyond Numeracy"
subtitle: "Ruminations of a Number Man"
authors:
  - "John Allen Paulos"
read_by: ~
date: 2020-12-05T09:32:08-05:00
tags:
  - mathematics
  - popular mathematics
categories:
draft: false
---

Paulos is most famous for a previous book, *Innumeracy*. That book is
a call to treat lack of basic mathematical knowledge and intution with
the same seriousness that society has for illiteracy.

This book is of a different nature. It examines many of the typical
examples of interesting bits of mathematics in an attempt to
demonstrate why mathematicians become and are interested in
mathematics. So it's not very detailed in any sense, but the reader
gets a glimpse of things like non-Euclidean geometry, Moebius strips,
primes, etc. in very small, bite-sized chapters that simple intend to
impart a bit of the flavor of those topics in such a way to show why
someone might think they were interesting.

As I write this I try to remember what of these types of things took
hold in me when I was young, and I confess it was so long ago that I
can't really remember for sure. I do recall, hopefully with a true
memory, finding out about Fermat's Last Theorem, then still unproved,
and taking a naive whack at the issues around it, as I'm sure many
youngsters at that time who encountered it did. And after training in
mathematics it's hard to look back at that time the same - complex
numbers don't hold the same mysteries for me anymore that they did in
my teen years, though there are plenty of new ones to replace those
(is there anything interesting to be had in considering unbounded
composition operators on the Hardy space on the one dimensional disk
by allowing symbols that map outside of the disk? I'll wonder about
that one for a long time - maybe sometime I'll find the time and
energy to investigate it myself, but as far as I can tell no one else
has).
