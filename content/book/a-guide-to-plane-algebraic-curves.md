---
title: "A Guide to Plane Algebraic Curves"
subtitle: ""
authors:
  - "Keith Kendig"
date: 2019-06-14T11:00:29-05:00
tags:
  - mathematics
  - geometry
  - algebraic geometry
  - Riemann surfaces
  - algebraic curves
categories:
draft: false
---

A long time ago when I was a graduate student studying mathematics I
took courses in such things as differential geometry and Riemann
surfaces and audited one in general algebraic geometry. In some sense
I did not get as much as possible out of those courses because I had
not really engaged with the at a lower level first. For example, I
could do the calculations in involved in Riemannian geometry, but the
geometric intuition was lacking because I had never really studied the
differential geometry of curves in surfaces in two and three
dimensions, as one might do in an undergraduate course.

This book of Kendig's is a largely intuitive guide to plane algebraic
curves, about the simplest objects one encounters in algebraic
geometry.  Anyone who has graphed a line or parabola in pre-algebra
has (perhaps unwittingly) engaged in a bit of algebraic geometry,
which in a rough sense is the study of the relationship between zeros
of polynomials and the algebraic properties of those polynomials.

That can become incredibly abstract and complicated - see
Grothendieck's line of work in algebraic geometry for proof of
that. However, there remains the basic intuitive ideas that come from
things like conic sections and Riemann surfaces, and this is where
Kendig goes. There is little proof in the book, but plenty of
illustration and ideas. We see things like how to create a plane curve
that has certain properties, or how to think about what is happening
at singularities and intersections. We see a variety of important
examples of plane curves. The geometric aspects shine through here,
and we get intuitive approaches to things like Bézout's theorem and
the Riemann-Roch theorem. We get a good discussion of conics. There is
discussion of the relationship between the various algebraic,
geometric, and analytic view of Riemann surfaces.

In short, this is the kind of thing that would have helped fill in
the intuition that I was missing about these things. And I think 
having read it will be helpful as I try to learn a little bit of
algebraic geometry for real (though for fun) in my current life.
