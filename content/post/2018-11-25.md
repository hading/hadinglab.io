---
title: "Someone else and everyone"
date: 2018-11-25
draft: false
---

Not too long ago Paul Allen, the co-founder of Microsoft, died. In
remembrance Bill Gates said something to the effect that personal
computing would not have come about without the efforts of Allen.

While I don't wish to minimize Gates's tribute to his friend, this is
absurd and the fact that we see this kind of thinking is detrimental
to society. If Paul Allen had not been in that position, we cannot say
exactly how personal computing would have come about. What we can say
with perfect surety is that it simply would have been _someone
else_. The ingredients were there, and someone would have done
it. Einstein is justly famous for his physical theories, but if he
hadn't come up with relativity, it just would have been someone
else. The magic is not in these individuals, but in having the type of
society that is able to produce them, or in their absence, someone
else who can do the same thing. To think to the contrary, that there
is something incredibly unique in them that their absence would deny
to the worlk is, I think, destructive. It focuses our attention on
them instead of the problems of maintaining and improving the society
that is able to produce them, and it diminishes the role of _everyone_.

By this I mean the idea that, though Einstein wrote down the theories,
it seems absurd to downplay the fact that being set in a society meant
that he didn't have to grow or hunt his own food, take care of his own
illnesses, make his own scientific instrumens, construct his own
vehicles, etc. ad infinitum. And this applies generally. There seems
little reason to overvalue the one particular person when without the
supporting cast of the entire society the accomplishments of that
person are impossible.

And that insight plays a crucial role in my feelings about our current
capitalistic, overly business-centric society.
