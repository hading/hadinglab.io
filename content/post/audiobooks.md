---
title: "Audiobooks"
date: 2019-10-12T11:33:59-04:00
tags: 
categories: 
  - audiobook
draft: false
---

Now that I've acquired a substantial driving commute to work I have 
started listening to audiobooks during that time. So I decided that
I would start posting reviews of those as well. I have a few waiting
to do now and will try to knock them out soon, though some time has
elapsed and so some might be brief.

