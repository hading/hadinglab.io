---
title: "The Economy"
date: 2020-03-25T09:34:49-04:00
tags:
  - economics
  - society
categories:
draft: false
---

Well, in these days we're hearing a lot of talk about the balance
between containing coronavirus and the effects on the economy. Perhaps
the best quote that I've read was something to the effect of - if we
need millions of people to die for the economy to work, then the
economy is not working.

This is basically a drum that I've been beating for some time in a
broader context. That is, you hear things like - we can't tackle this
or that environmental issue (be it small or climate change) because it
would be bad for the economy. We can't pay a living wage, because it
would be bad for the economy. We can't assure people have healthcare
or give everyone the tools they need to have a happy and fulfilling
life, because...  you guessed it.

At what point do you admit that perhaps the economy is not something
to be served and protected, but is itself the problem? When do you
admit that an economy is just a tool that can be used or discarded in
favor of another economy (again a tool) to achieve the _actual_ things
that society wants. To be clear, in my view the economy is _not_ a
primary concern. It is only something that you _use_ to achieve your
real goals.

However, it's obviously a very complex and unpredictable thing. So it
is to be expected that it will require constant maintaining. You have
to constantly evaluate how well it is helping you fulfill your goals. 
When and where it is not, it must be adjusted. It may have side effects
that were not (and maybe could not) be foreseen. So it may require 
adjustment or replacement for that reason.

I think that any reasonable evaluation of capitalism has to face this.
I may rag on it, but clearly it did a good job of replacing what came
before it and did in fact make life better for more people. That
doesn't mean that it can work forever, just that it helped in a
certain context. Now more than ever it's obvious what problems it
doesn't solve and what it actually _creates_. So a reasonable society
would take stock of that and modify or replace to handle those things.
There isn't actually any reason, I think, to believe that there is a
particular "ideal" economic system that can be reached, rather one
needs to look at the current context and see what needs to happen
there. But people seem loath to embrace this idea.

And to veer a little bit, once when I was in graduate school I was
sitting at my desk and I wrote down a proof that every function is
analytic. Now, this is somewhat absurd, as not every function _is_ in
fact analytic. In fact almost none of them are by any reasonable
measure. My reaction was not to double down on the analyticity of all
functions. Instead, I thought to myself, well, that can't be right,
and set about finding where the error in my "proof" was. I wonder how
it is that some people can say things like seniors should be willing
to sacrifice their lives for the economy without stopping, saying to
themselves, no that doesn't sound quite right, and retracing their
reasoning to see where the error is. Well, I suspect that there isn't
any reasoning, just hewing to ideology. So maybe I do know.
